--------------------------
ResetPassword Java Servlet
--------------------------

This software is free and open source, so it has no warranty.

------------
Requirements
------------

 - Java 1.6
 - Ant/Maven

Note: The servlet is currently set to be compiled with Java 7 and
target Java 1.6 in the build.xml file.  If you need to change this,
then modify the ant.build.javac.source and target properties as well
as the lib.path.ref directory path for rt.jar.

-----------------------
Pre-Build Configuration
-----------------------

The servlet uses three configuration files, two are compiled into the
WAR file and loaded on servet init, the other is the connection
information that is loaded each time the servlet runs.

  1) custom/default.properties (compiled into the WAR file)

    a) You must change the properties_path to point to the location of
       the ResetPassword.properties file on your server

    b) Define the error messages that the servlet will display.

    c) OPTIONAL.  You can also define the some of the same settings as
       ResetPassword.properties below that act as global settings for
       when you have multiple Tomcat servers, for example, and only
       want to define the differences in the server properties file.
       Do not put passwords here though.

  2) custom/school.properties (compiled into the WAR file)

    a) This file defines the verification and validation fields on
       each page in the form.  Because the servlet now supports
       multiple schools, there can be many different settings.  You
       must have at least one school defined.

    b) Modify the sample file to fit your institution

    c) For each school there are 4 sections that have to be
       configured:

      i) title = the display name of the school

      ii) identity = the location of the username and PERSON ID, valid
                     options are ldap or unidata

      iii) verify = defines the fields used for verification
                   (i.e. birth date, zip code)

        A) Verification fields have additional data members that
           define the location in unidata or ldap

      iv) challenge = the location where the challenge question and
                      answer are stored, either ldap or unidata

  3) ResetPassword.properties (stored on the web server)

    a) This contains your connection information to Active Directory
       and UniData and should be kept in a secure place on your
       server.  On Linux servers I put it in /etc and make sure it is
       readable only by the web server user account.

    b) See the sample ResetPassword.properties file in this directory.
       You can copy it to your server as the basis for the production
       settings.

    c) Make sure the path in default.properties points to the correct
       path or the connection to Colleague and Active Directory will
       fail.

You can also further customize the look with the HTML and CSS files in
the WebContent directory.

  1) WebContent/*.html

    a) The HTML files will need to be customized to point to your
    school's logos and also to contain the correct dialog messages.

      i) dialogs.html = each of these dialogs must have your school
                        code, defined in school.properties, appended
                        on the end.  For example, I defined two
                        schools, cfcc and ncwc, I must have a set of
                        dialogs with -cfcc and -ncwc set up in this
                        file.

      ii) footer.html = This contains the school's phone numbers and
                        other contact information.

      iii) header.html = this contains any content that you want
                         appear at the top of the page

  2) WebContent/css/theme.css

    a) This is where most of the themes are defined.  The most
    important line is the "masthead" class that defines a link to your
    school's logo.

---------------------
Building the WAR file
---------------------

Two methods are available to build the libraries, Ant and Maven.  Currently
I'm using Maven to take advantage of the ability to automatically include
files and projects as dependencies without having to rely on the IDE.  It
does introduce complexity because you either have to have a local repository
on your development machine or set one up on the network (for example: Nexus)
and then "install" the packages into it.

Currently Nexus has been loaded on vault.ad.cfcc.edu and the ColFiles and
ColleagueLdapTools projects are set up to send their packages there.

Maven
-----

All the setup for Maven for this project is in the pom.xml file and the
settings.xml file in your home directory (~/.m2).

To build the WAR file, from the project directory:

    mvn package

Ant
---

The servlet project, ResetPassword, depends on two jar files provided
by the projects, ColFiles and ColleagueLdapTools.  As long as all
three projects are in the same top level directory (for example
Eclipse's ~/workspace/ directory), then ant will know how to find the
correct libraries.

My workflow is usually as follows (this is on a Linux workstation):

    cd ~/workspace
    cd ColFiles
    ant
    cd ../ColleagueLdapTools
    ant
    cd ../ResetPassword
    ant

The result with be a WAR file in the build subdirectory in the
ResetPassword directory.

-------
Archive
-------

To generate an Zip file export of this project, run Ant with the following target:

    ant -f build-custom.xml archive

Additional JAR files can be copied from the target/ResetPassword*/WEB-INF/lib folder built by Maven.

----------
Deployment
----------

Deploy the file to a Tomcat server in the usual way.  There are some
sample catalina.jar xml files, rp-deploy.xml and tomcat-common.xml,
that you can use if you have multiple Tomcat servers set up to accept
script deployments.

For example:

    ant -f rp-test.xml redeploy

Note: When using Maven to build the WAR file, the rp-*.xml must be updated
      for each new version until I am able to integrate it so it pulls the
      version information from Maven.

Copy your modified ResetPassword.properties to the server so that the
servlet can find it.

--
Updated: 2016-12-30