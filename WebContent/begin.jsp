<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@ page import="java.util.TreeMap" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- See http://java.sun.com/developer/technicalArticles/javaserverpages/jsp_templates/ -->
<head>
<meta name="author" content="Jakim Friant">
<meta name="copyright" content="2012 Cape Fear Community College">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<title><%=application.getAttribute("title")%></title>
<link type="text/css" href="css/redmond/jquery-ui-1.10.3.custom.css"
	rel="Stylesheet" />
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<link type="text/css" href="css/basic.css" rel="Stylesheet" media="all">
<link type="text/css" href="css/theme.css" rel="Stylesheet" media="all">
<%
	if (application.getAttribute("css_url") != null) {
		out.print("<link type=\"text/css\" href=\""
				+ application.getAttribute("css_url")
				+ "\" rel=\"Stylesheet\" media=\"all\">");
	}
%>
<script type="text/javascript" src="js/validate.js"></script>
<script type="text/javascript" src="js/dlgusername.js"></script>
<script type="text/javascript" src="js/multischool.js"></script>
<script type="text/javascript">
// <!-- Set up the school info so the Javascript function can use it later -->
<%=application.getAttribute("school_info") %>
function beginOnLoad() {
	schoolChange();
	setFocus('txt_username');
};
</script>
</head>
<body onload="beginOnLoad()">
	<div id="page">
		<jsp:include page="header.html" />
		<jsp:include page="dialogs.html" />
		<div id="content">
			<h1><%=application.getAttribute("title")%></h1>
			<form id="simpleform" autocomplete="off"
				action='<%=response.encodeURL("ResetPassword")%>' method="post"
				onsubmit="return checkStartForm();">
				<fieldset>
					<legend>Enter Your Information</legend>
					<input type="hidden" name="step" value="start" />
					<%
						String is_passwordhelp = (String) application
								.getAttribute("passwordhelp");
						if (is_passwordhelp != null && is_passwordhelp.equals("true")) {
							out.write("<input type=\"hidden\" name=\"passwordhelp\" value=\"true\">");
						}
					%>
					<!-- If there are multiple schools in the config, then we'll display a school name
						 drop-down with the host school listed first -->
					<%
					if (application.getAttribute("multischool").equals("yes")) {
					%>
					<div class="questionBox">
						<div class="questionBoxLabel">
							<label for="sel_school">Select School: </label>
						</div>
						<div class="questionBoxInput">
					<%
						// create the school dropdown
						@SuppressWarnings("unchecked")
						TreeMap<String, String> school_titles = new TreeMap<String, String>((HashMap<String, String>) application.getAttribute("school_titles"));
						Iterator<String> school_codes = school_titles.keySet().iterator();
						String cur_code = (String) application.getAttribute("school_code");
						out.write("<select id=\"sel_school\" name=\"sel_school\" onChange=\"schoolChange()\" class=\"ui-widget-content ui-corner-all\">\n");
						while (school_codes.hasNext()) {
							String sc = school_codes.next();
							String selected = ""; 
							if (sc.equals(cur_code)) {
								selected = " selected";
							}
							out.write("<option value=\"" + sc + "\"" + selected + ">" + school_titles.get(sc) + "</option>\n");
						}
						out.write("</select>\n");
					}
					%>
						</div>
					</div>
					<div class="questionBox">
						<div class="questionBoxLabel">
							<label id="lbl_username" for="txt_username">CFCC User Name: </label>
						</div>
						<div class="questionBoxInput">
							<input type="text" name="txt_username" size="25"
								id="txt_username" class="text ui-widget-content ui-corner-all"
								tabindex="1" />
							<button type="button" id="what-username">What's my Username?</button>

							<div class="ui-state-error toggle" id="msg_uid_blank">You
								must enter your user name</div>
							<div class="ui-state-error toggle" id="msg_uid_error">Invalid
								user name</div>

						</div>
						<div class="questionBoxLabel">
							<label id="lbl_person_id" for="txt_per_id">CFCC ID Number: </label>
						</div>
						<div class="questionBoxInput">
							<input type="text" name="txt_per_id" size="25" maxlength="7"
								id="txt_per_id" class="text ui-widget-content ui-corner-all"
								tabindex="2" />
							<button type="button" id="what-idnumber">What's my ID Number?</button>
							<div class="ui-state-error toggle" id="msg_per_id_error">Must
								be numbers only</div>
						</div>
					</div>
				</fieldset>
				<p id="validate_msg" class="toggle btn">
					<span class="ui-state-error">You must correct the errors
						first</span>
				</p>
				<p class="btn">
					<input type="submit" id="cmd_submit" name="cmd_submit" value="Next">
				</p>
			</form>
			<p class="copyright">
				<%=application.getAttribute("version")%>
				<%
				Boolean debug = (Boolean) application.getAttribute("debug");
				if (debug) {
					String build_id = (String) application.getAttribute("build"); 
					out.append(" (build ").append(build_id).append(")");
					String which_env = (String) application
							.getAttribute("environment");
					if (which_env == null) {
						which_env = "Unknown";
					}
					out.append("<br><br><span class=\"debug\">Connected to ")
							.append(which_env).append("</span>\n");
				}
			%>
			</p>
		</div>
		<jsp:include page="footer.html" />
	</div>
</body>
</html>
