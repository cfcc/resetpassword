<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=application.getAttribute("title")%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<link type="text/css" href="css/redmond/jquery-ui-1.10.3.custom.css"
	rel="Stylesheet" />
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<link type="text/css" href="css/basic.css" rel="Stylesheet" media="all">
<link type="text/css" href="css/theme.css" rel="Stylesheet" media="all">
<%
	if (application.getAttribute("css_url") != null) {
		out.print("<link type=\"text/css\" href=\""
				+ application.getAttribute("css_url")
				+ "\" rel=\"Stylesheet\" media=\"all\">");
	}
%>
<script type="text/javascript" src="js/passwordvalidator.js"></script>
<script type="text/javascript" src="js/validate.js"></script>
<script type="text/javascript">
            setRequirements(<%=session.getAttribute("passwd_min_len")%>, <%=session.getAttribute("passwd_max_len")%>, <
	%=
session.getAttribute("passwd_no_special")%>
	
);
	$(function() {
		$("input:submit", "#simpleform").button();
	});
</script>
</head>
<body onload="setFocus('txt_passwd')">
    
    <div id="page">
      <jsp:include page="header.html" />
        <div id="content">
            <h1>Enter a New Password</h1>
            <p>Please choose a new password, it must be at least <%=session.getAttribute("passwd_min_len")%> characters in length, must must have a mix of upper and lower case letters, and have at least one number.</p>
            <p>Please note that passwords cannot include your any part of your name.</p>
			<%
				String msg = (String) session.getAttribute("password_result");
				if (msg != null) {
			%>
			<div class="alert">
				<p>
					<b>Password is not valid:</b>
				</p>
				<%=msg%>
			</div>
			<%
				}
			%>
			<form id="simpleform" autocomplete="off" action="<%=response.encodeURL("ResetPassword")%>" method="post" onSubmit="return checkPassword('submitMessage')">
                <fieldset>
                    <legend>Enter Your Information</legend>
                    <input type="hidden" name="step" value="change">
                    <input type="hidden" name="txt_per_id" value="<%=session.getAttribute("person_id")%>">
                    <div class="questionBox">
                        <div class="questionBoxLabel">
                        	<label for="txt_passwd">New Password:</label>
                        </div>
                        <div class="questionBoxInput">
                            <input type="password" name="txt_passwd" id="txt_passwd" size="30" class="text ui-widget-content ui-corner-all" />
                            <script type="text/javascript">createPasswordValidator("txt_passwd");</script>
                            <div id="error_msg_box"></div>
                        </div>
                        <div class="questionBoxLabel">
                        	<label for="txt_passwd_check">Confirm Password:</label>
                        </div>
                        <div class="questionBoxInput">
                        	<input type="password" name="txt_passwd_check" id="txt_passwd_check" size="30" class="text ui-widget-content ui-corner-all" /><br>
                            <span class="error toggle" id="msg_match_error">Passwords must match!</span>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="securityQuestion">
                        <fieldset>
                            <legend>Security Question</legend>
                            <div class="questionBox">
                                <div class="questionBoxLabel">
                                    <select name="sel_question" id="sel_question" onchange="clearField('txt_secret')">
										<%
											String[] questions = (String[]) session
													.getAttribute("security_questions");
											String curr_question = (String) session
													.getAttribute("curr_question");
											for (int i = 0; i < questions.length; i++) {
												if (questions[i].equals(curr_question)) {
													out.println("<option selected>" + questions[i]
															+ "</option>");
												} else {
													out.println("<option>" + questions[i] + "</option>");
												}
											}
										%>
									</select>
                                </div>
                                <div class="questionBoxInput">
                                    <input type="text" name="txt_secret" id="txt_secret" size="25" onBlur="checkSecret(this)" maxlength="<%=session.getAttribute("secret_len")%>" value="<%=session.getAttribute("secret")%>" class="text ui-widget-content ui-corner-all" /><br>
                                    <span class="error toggle" id="msg_secret_error">You must enter a security question.</span>
                                </div>
                            </div>
                        </fieldset>
                    </div>

					<p id="validate_msg" class="alert toggle btn">You must correct the errors first.</p>
                    <p class="btn"><input type="submit" name="cmd_change_passwd" id="cmd_change_passwd" value="Finished"></p>
                    <div id='submitMessage' class="clean-ok toggle"></div>
                </fieldset>
            </form>

            <p class="copyright"><%=application.getAttribute("version")%></p>
            
        </div>
	<jsp:include page="footer.html" />
        </div>
    </body>
</html>
