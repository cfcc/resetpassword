<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=application.getAttribute("title")%></title>
<link type="text/css" href="css/redmond/jquery-ui-1.10.3.custom.css" rel="Stylesheet">
<link type="text/css" href="css/theme.css" rel="Stylesheet" media="all">
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<link type="text/css" href="css/basic.css" rel="Stylesheet" media="all">
<script type="text/javascript" src="js/onerror.js"></script>
</head>
<body>
	<div id="page">
		<jsp:include page="header.html" />
		<div id="content">
			<div id='statusMessage'>
				<div id='errorTitle' class="ui-state-highlight btn">
					<b>Error!</b>
				</div>
				<%=(String) application.getAttribute("error_msg")%>
				<%
					Boolean debug = (Boolean) application.getAttribute("debug");
					if (debug.booleanValue() == true) {
						String log_msg = (String) application.getAttribute("log_msg");
						out.append("<p class=\"debug\">").append(log_msg)
								.append("</p>\n");
					}
				%>
				<p class="status">
					<a href="<%=(String) application.getAttribute("link")%>">Start over...</a>
				</p>
			</div>
		</div>
		<jsp:include page="footer.html" />
	</div>
</body>
</html>
