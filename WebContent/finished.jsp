<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<title><%=application.getAttribute("title")%></title>
<link type="text/css" href="css/redmond/jquery-ui-1.10.3.custom.css"
	rel="Stylesheet" />
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="js/onerror.js"></script>
<link type="text/css" href="css/basic.css" rel="Stylesheet" media="all">
<link type="text/css" href="css/theme.css" rel="Stylesheet" media="all">
<%
	if (application.getAttribute("css_url") != null) {
		out.print("<link type=\"text/css\" href=\""
				+ application.getAttribute("css_url")
				+ "\" rel=\"Stylesheet\" media=\"all\">");
	}
%>
</head>
<body>
	<div id="page">
		<jsp:include page="header.html" />
		<div id="content">
			<div id='statusMessage'>
				<h1>Success</h1>

				<p>Your password in the Directory Server was updated.</p>

				<%
					String warning_msg = (String) application.getAttribute("warning");
					if (warning_msg != null) {
						out.print("<div class=\"warning\">" + warning_msg + "</div>\n");
					}
				%>
				<!-- TODO: Need a bold message about restarting the computer for the PasswordHelp page -->
				<%
					String is_passwordhelp = (String) application
							.getAttribute("passwordhelp");
					if (is_passwordhelp != null && is_passwordhelp.equals("true")) {
				%>
				<div class="clean-ok">
					<h3>Please log off or restart your computer now</h3>
				</div>
				<%
					} else {
				%>
				<p class="status">
					<a href="<%=application.getAttribute("server_url")%>/">Continue
						to <%=application.getAttribute("server_name")%>...
					</a>
				</p>
				<%
					}
				%>
				<hr>

				<p class="copyright"><%=application.getAttribute("version")%></p>
			</div>
		</div>
		<jsp:include page="footer.html" />
	</div>
</body>
</html>
