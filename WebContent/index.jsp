<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String hostName = request.getServerName();
	int port = request.getServerPort();
	if (port != 80 || port != 443) {
		hostName = hostName + ":" + String.valueOf(port);
	}
	String redirectUrl = "https://" + hostName + "/ResetPassword/ResetPassword"; 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<META HTTP-EQUIV=REFRESH
	CONTENT="0; URL=https://<%=hostName%>/ResetPassword/ResetPassword" />
<link type="text/css" href="css/basic.css" rel="Stylesheet" media="all">
<link type="text/css" href="css/theme.css" rel="Stylesheet" media="all">
<TITLE>ResetPassword</TITLE>
</head>
<body>
	<div id="page">
		<jsp:include page="header.html" />
		<div id="content">
			<p>
				You should be automatically redirected to the <a
					href="<%=redirectUrl%>">Reset
					Password</a> page, if not click the link to continue.
			</p>
		</div>
		<jsp:include page="footer.html" />
	</div>
</body>
</html>
