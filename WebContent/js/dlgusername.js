$(function() {
	$("input:submit", "#simpleform").button();
	var loadUrl = "/ResetPassword/UserName",
	txt_username = $("#txt_username"),
	txt_per_id = $("#txt_per_id"),
	per_last = $("#per_last"),
	per_id = $("#per_id"),
	per_ssn = $("#per_ssn");
	var all_fields = $([]).add(per_last).add(per_id).add(per_ssn);
	function checkLength(obj, field_name, min, max) {
		if ( obj.val().length > max || obj.val().length < min ) {
			obj.addClass("ui-state-error");
			return false;
		} else {
			return true;
		}
	}
	$("#dialog_username-cfcc").dialog({
		autoOpen : false,
		width : 600,
		modal : true,
		buttons : {
			"Continue" : function() {
				txt_username.val("Loading...");
				txt_per_id.val("");
				// check the input from the user name dialog
				all_fields.removeClass("ui-state-error");
				var bValid = true;
				bValid = bValid && checkLength( per_last, "last name", 1, 26 );
				bValid = bValid && (checkLength( per_id, "person ID", 7, 7 ) ||
						checkLength( per_ssn, "Social Security Number", 9, 12 ));
				if (bValid) {
				$.post(loadUrl,
						{
							per_last : $("#per_last").val(),
							per_id : $("#per_id").val(),
							per_ssn : $("#per_ssn").val()
						},
						function(responseText) {
							var obj = jQuery.parseJSON(responseText);
							if ( obj.success == true ) {
								$("#txt_username").val(obj.username);
								$("#txt_per_id").val(obj.person_id);
								$("#per_last").val("");
								$("#per_id").val("");
								$("#per_ssn").val("");
							} else {
								$("#txt_username").val("");
								$("#dialog_findusername_error").dialog("open");
							}
							checkStartForm();
						},
						"text");
				$(this).dialog("close");
				}
			},
			Cancel : function() {
				txt_username.val("");
				txt_per_id.val("");
				per_last.val("");
				per_id.val("");
				per_ssn.val("");
				$(this).dialog("close");
			}
		},
		close : function() {
		}
	});

	$("#dialog_username-ncwc").dialog({
		autoOpen: false,
		modal: true,
		width: 600,
		buttons: {
			Ok: function() {
				$(this).dialog("close");
			}
		}
	});

    $("#dialog_username-trustee").dialog({
        autoOpen: false,
        modal: true,
        width: 600,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
            }
        }
    });

    $("#what-username").button().click(function() {
		var school_select = document.getElementById("sel_school");
		var school_code = school_select.options[school_select.selectedIndex].value;
		var which_form = "#dialog_username-" + school_code;
		$(which_form).dialog("open");
	});

	$("#what-idnumber").button().click(function() {
		var school_select = document.getElementById("sel_school");
		var school_code = school_select.options[school_select.selectedIndex].value;
		var which_form = "#dialog_id_number-" + school_code;
		$(which_form).dialog("open");
	});
	
	$("#dialog_id_number-cfcc").dialog({
		autoOpen: false,
		modal: true,
		width: 600,
		buttons: {
			Ok: function() {
				$(this).dialog("close");
			}
		}
	});

	$("#dialog_id_number-ncwc").dialog({
		autoOpen: false,
		modal: true,
		width: 600,
		buttons: {
			Ok: function() {
				$(this).dialog("close");
			}
		}
	});

    $("#dialog_id_number-trustee").dialog({
        autoOpen: false,
        modal: true,
        width: 600,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog_findusername_error").dialog({
		autoOpen: false,
		modal: true,
		width : 600,
		buttons: {
			Ok: function() {
				$(this).dialog("close");
			}
		}
	});
});
