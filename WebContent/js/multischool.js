function schoolChange() {
	var school_select = document.getElementById("sel_school");
	var school_code = school_select.options[school_select.selectedIndex].value;
	if (school_info.hasOwnProperty(school_code)) {
		document.getElementById("lbl_username").innerHTML = school_info[school_code]["username"]["description"];
		document.getElementById("txt_username").setAttribute("maxlength", school_info[school_code]["username"]["length"]);
		document.getElementById("lbl_person_id").innerHTML = school_info[school_code]["person_id"]["description"];
		document.getElementById("txt_per_id").setAttribute("maxlength", school_info[school_code]["person_id"]["length"]);
	}
}