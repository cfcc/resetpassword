// -*- Java -*-
//
// Validate.js -- functions to validate the form before submission
//

//---
// Final processing message before the user is shown the finished page:
var M_PROCESSING = "Please wait, your information is being updated...";

//------------------------------------------------------------------------
// this doesn't validate but sets the focus
var formInUse = false;

function setFocus(fieldname)
{
    target = document.getElementById(fieldname);
    if(!formInUse) {
        target.focus();
    }
}

//------------------------------------------------------------------------
function toggle(target_id, show) {
	target = document.getElementById(target_id);
	if (target) {
		if (show == 1) {
			target.style.display = "block";
		} else {
			target.style.display = "none";
		}
	}
}

//------------------------------------------------------------------------
function trim1 (str) {
    return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

//------------------------------------------------------------------------
function isNum(passedVal) {
    // check to see if the value is a number
    if (passedVal == "") {
        return false;
    }
    for (var i = 0; i < passedVal.length; i++) {
        if (passedVal.charAt(i) < "0") {
            return false;
        }
        if (passedVal.charAt(i) > "9") {
            return false;
        }
    }
    return true;
}

//------------------------------------------------------------------------
function checkDate(field) {
    // This script and many more are available free online at
    // The JavaScript Source!! http://javascript.internet.com
    // Original:  Torsten Frey (tf@tfrey.de)
    // Web Site:  http://www.tfrey.de
    // 
    // Modified by J.Friant to accept US format dates with additional ideas from
    // the DHTML date validation script. Courtesy of SmartWebby.com
    // (http://www.smartwebby.com/dhtml/)
    //
    var checkstr = "0123456789";
    var DateField = field;
    var DateValue = "";
    var DateTemp = "";
    var seperator = "/";
    var day;
    var month;
    var year;
    var leap = 0;
    var err = 0;
    var i;
    var intYear = 0;
    var yrPrefix = "";
    var re = new RegExp("[/.]", "g");
    
    err = 0;

    // This is no longer a required field, so don't check blank dates
    if (DateField == null || DateField.value == "") {
        return true;
    }
	DateValue = DateField.value;

	// Standardize the separators
    var dtStr = DateValue.replace(re, seperator);
    
    // first try to find the separators (if any)
    var pos1=dtStr.indexOf(seperator);
    var pos2=dtStr.indexOf(seperator,pos1+1);
    
    if (pos1 != -1) {
        var strMonth=dtStr.substring(0,pos1);
        var strDay=dtStr.substring(pos1+1,pos2);
        var strYear=dtStr.substring(pos2+1);
        if (strMonth.length < 2) { 
            strMonth = '0' + strMonth;
        }
        if (strDay.length < 2) { 
            strDay = '0' + strDay;
        }
        DateValue = strMonth + strDay + strYear;
    }
    
    /* Delete all chars except 0..9 */
    for (i = 0; i < DateValue.length; i++) {
        if (checkstr.indexOf(DateValue.substr(i,1)) >= 0) {
            DateTemp = DateTemp + DateValue.substr(i,1);
        }
    }
    DateValue = DateTemp;
    /* Always change date to 8 digits - string
    *
    * If year is entered as 2-digit then treat all years less than 21 as
    * 20xx and everything else as 19xx
    */
    if (DateValue.length == 6) {
        intYear = parseInt(DateValue.substr(4,2));
        if (intYear < 20) {
            yrPrefix = '20';
        }
        else {
            yrPrefix = '19';
        }
        DateValue = DateValue.substr(0,4) + yrPrefix + DateValue.substr(4,2);
    }
    if (DateValue.length != 8) {
        err = 19;
    }
    /* year is wrong if year = 0000 */
    year = DateValue.substr(4,4);
    if (year == 0) {
        err = 20;
    }
    /* Validation of month*/
    month = DateValue.substr(0,2);
    if ((month < 1) || (month > 12)) {
        err = 21;
    }
    /* Validation of day*/
    day = DateValue.substr(2,2);
    if (day < 1) {
        err = 22;
    }
    /* Validation leap-year / February / day */
    if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
        leap = 1;
    }
    if ((month == 2) && (leap == 1) && (day > 29)) {
        err = 23;
    }
    if ((month == 2) && (leap != 1) && (day > 28)) {
        err = 24;
    }
    /* Validation of other months */
    if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
        err = 25;
    }
    if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
        err = 26;
    }
    /* if 00 is entered, no error, deleting the entry */
    if ((day == 0) && (month == 0) && (year == 00)) {
        err = 0; day = ""; month = ""; year = ""; seperator = "";
    }
    /* if no error, write the completed date to Input-Field (e.g. 13.12.2001) */
    if (err == 0) {
        DateField.value = month + seperator + day + seperator + year;
        toggle("msg_birth_date_error", 0);
    }
    /* Error-message if err != 0 */
    else {
        toggle("msg_birth_date_error", 1);
    }
    return (err == 0);
}

//-------------------------------------------------------------------------
function check_birth_date(field) {
	return checkDate(field);
}

//-------------------------------------------------------------------------
function check_zip_code(field) {
    var okay = false;
    inZip = field.value;
    if ((isNum(inZip) == false) || (inZip.length != 5)) {
        toggle("msg_zip_code_error", 1);
    } else {
        toggle("msg_zip_code_error", 0);
        okay = true;
    }
    return okay;
}

//-------------------------------------------------------------------------
function checkPerId(field) {
    var okay = false;
    inId = field.value;
    if ((isNum(inId) == false) || (inId.length < 1)) {
        toggle("msg_per_id_error", 1);
    } else {
        toggle("msg_per_id_error", 0);
        okay = true;
    }
    return okay;
}

//------------------------------------------------------------------------
function check_ssn_four(field) {
    var okay = false;
    if (field != null) {
    	inSsn = field.value;
    	if ((inSsn == "") || ((inSsn.length == 4) && (isNum(inSsn) == true))) {
    		okay = true;
    		toggle("msg_ssn_four_error", 0);
    	} else {
    		toggle("msg_ssn_four_error", 1);
    	}
    } else {
    	okay = true;
    }
    return okay;
}

//------------------------------------------------------------------------
function checkUsername(field) {
    var okay = false;
    toggle("msg_uid_blank", 0);
    toggle("msg_uid_error", 0);
    var username = trim1(field.value);
    if (username != "") {
        if (username.indexOf(" ", 1) == -1) {
            okay = true;
        }
        else {
            toggle("msg_uid_error", 1);
        }
    }
    else {
        toggle("msg_uid_blank", 1);
    }
    return okay;
}

//------------------------------------------------------------------------
function checkValidationForm() {
	// A final check before submitting the form
    okay = false;
	toggle("validate_msg", 0);
	var result1 = check_ssn_four(document.getElementById("txt_ssn_four"));
	var result2 = check_birth_date(document.getElementById("txt_birth_date"));
	var result3 = check_zip_code(document.getElementById("txt_zip_code")); 
    if ( result1 && result2 && result3 ) {
        okay = true;
    } else {
    	toggle("validate_msg", 1);
    }
    return okay;
}

//------------------------------------------------------------------------
function checkStartForm() {
    okay = false;
	toggle("validate_msg", 0);
	toggle("msg_uid_blank", 0);
	toggle("msg_uid_error", 0);
	toggle("msg_per_id_error", 0);
	if (checkUsername(document.getElementById("txt_username")) &&
        checkPerId(document.getElementById("txt_per_id"))) {
        okay = true;
    } else {
    	toggle("validate_msg", 1);
    }
    return okay;
}

//------------------------------------------------------------------------
// The following are for the change_password page
//------------------------------------------------------------------------

//------------------------------------------------------------------------
function checkSecret(field) {
    var okay = false;
    secret = field.value;
    if (secret.length < 1) {
        toggle("msg_secret_error", 1);
    }
    else {
        toggle("msg_secret_error", 0);
        okay = true;
    }
    return okay;
}

//------------------------------------------------------------------------
function checkPassword(messageTag) {
    okay = false;
	toggle("validate_msg", 0);
    if ((validatePassword("txt_passwd") > 0) && checkSecret(document.getElementById("txt_secret")))
    {
        var passwd = document.getElementById("txt_passwd").value;
        var passwd_check = document.getElementById("txt_passwd_check").value;
        if (passwd == passwd_check) {
            okay = true;
            toggle("msg_match_error", 0);
        }
        else {
            toggle("msg_match_error", 1);
        }
    }
    else {
    	toggle("validate_msg", 1);
    }
    if (okay) {
    	// if the password passes validation, then we want to put up a message saying the results
    	// are being processed and instruct the user not to click the submit button again
    	document.getElementById(messageTag).innerHTML = M_PROCESSING; 
    	toggle(messageTag, 1);
    	// hide the error message if it's shown (ignored if not)
    	toggle('errorMessage', 0);
    }
    return okay;
}

//--------------------------------------------------------------------------
function clearField(field_id) {
    field = document.getElementById(field_id);
    field.value = "";
}

/*
 * Returns a new XMLHttpRequest object, or false if this browser doesn't
 * support it.
 */
function newXMLHttpRequest () {
    var xmlreq = null;

    if (window.XMLHttpRequest) {
        // create the request object in non-MS browsers
        xmlreq = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        // Create the request object via MS ActiveX
        try {
            xmlreq = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e1) {
            // failed to create the object
            try {
                // so try something supported by older versions
                xmlreq = new ActiveXObject("Microsoft.XMLHTTP");

            } catch (e2) {
            // Unable to create a request object with ActiveX
            }
        }
    }
    return xmlreq;
}

/*
 * Send a pair of passwords to the servlet for validation.
 */
function checkPasswordInteractive() {
    // get the XMLHttpRequest instance
    var req = new XMLHttpRequest();

    // set up the call-back function
    var handlerFunction = getReadyStateHandler(req, passwordMsg);
    req.onreadystatechange = handlerFunction;

    // open a HTTP POST connection back to the servlet in async mode
    req.open("POST", "ValidatePassword", true);

    // the body of the post is form data
    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    // send the data to the servlet
    var passwd = document.getElementById("txt_passwd").value;
    var passwd_check = document.getElementById("txt_passwd_check").value;
    req.send("password_first=" + passwd + "&password_check=" + passwd_check);
}

/*
 * Returns a function that waits for the specified XMLHttpRequest
 * to complete, then passes its XML response
 * to the given handler function.
 * req - The XMLHttpRequest whose state is changing
 * responseXmlHandler - Function to pass the XML response to
 * (From: http://www.ibm.com/developerworks/library/j-ajax1/)
 */
function getReadyStateHandler(req, responseXmlHandler) {

  // Return an anonymous function that listens to the
  // XMLHttpRequest instance
  return function () {

    // If the request's status is "complete"
    if (req.readyState == 4) {

      // Check that a successful server response was received
      if (req.status == 200) {

        // Pass the XML payload of the response to the
        // handler function
        responseXmlHandler(req.responseXML);

      } else {

        // An HTTP problem has occurred
        alert("HTTP error: "+req.status);
      }
    }
  };
}

//------------------------------------------------------------------------
function passwordMsg(error_xml) {
  var errors = error_xml.getElementsByTagName("errors")[0];
    var error_count = errors.getAttribute("count");
    if (error_count > 0) {
        var error_message = "";
        var all_msg = errors.getElementsByTagName("errmsg");
        for (var I = 0 ; I < all_msg.length ; I++) {
            error_message = error_message + all_msg[I].firstChild.nodeValue;
        }
        document.getElementById("error_msg_box").innerHTML = error_message;
    } else {
        document.getElementById("error_msg_box").innerHTML = "";
    }
}