<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@ page import="java.util.TreeMap" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" href="css/basic.css" rel="Stylesheet" media="all">
<link rel="stylesheet" href="css/jquery.mobile-1.3.2.min.css" />
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery.mobile-1.3.2.min.js"></script>
<title><%=application.getAttribute("title")%></title>
<script type="text/javascript" src="js/validate.js"></script>
<script>
function clearErrors() {
	toggle("validate_msg", 0);
	toggle("msg_uid_blank", 0);
	toggle("msg_uid_error", 0);
	toggle("msg_per_id_error", 0);
}
</script>
</head>
<body onload="clearErrors()">
	<!-- Home -->
	<div data-role="page" id="page1" data-theme="b">
		<div data-theme="b" data-role="header">
			<h3>Reset Password</h3>
		</div>
		<div data-role="content">
			<form id="simpleform" autocomplete="off" data-ajax="false"
				action='<%=response.encodeURL("ResetPassword")%>' method="post"
				onsubmit="return checkStartForm();">
				<input type="hidden" name="step" value="start" />
				<!-- If there are multiple schools in the config, then we'll display a school name
				 drop-down with the host school listed first -->
				<%
					if (application.getAttribute("multischool").equals("yes")) {
				%>
				<select id="sel_school" name="sel_school">
					<%
						// create the school dropdown
							@SuppressWarnings("unchecked")
							TreeMap<String, String> school_titles = new TreeMap<String, String>((HashMap<String, String>) application.getAttribute("school_titles"));
							Iterator<String> school_codes = school_titles.keySet().iterator();
							String cur_code = (String) application.getAttribute("school_code");
							while (school_codes.hasNext()) {
								String sc = school_codes.next();
								String selected = "";
								if (sc.equals(cur_code)) {
									selected = " selected";
								}
								out.write("<option value=\"" + sc + "\"" + selected + ">"
										+ school_titles.get(sc) + "</option>\n");
							}
					%>
				</select>
				<%
					}
				%>
				<div data-role="fieldcontain">
					<label for="txt_username" class="ui-hidden-accessible">Username:</label>
					<input name="txt_username" id="txt_username" placeholder="Username"
						value="" type="text">
					<div class="alert toggle" id="msg_uid_blank">You
						must enter your user name</div>
					<div class="alert toggle" id="msg_uid_error">Invalid
						user name</div>
					<label for="txt_per_id" class="ui-hidden-accessible">ID
						Number:</label> <input name="txt_per_id" id="txt_per_id"
						placeholder="ID Number" value="" type="text">
					<div class="alert toggle" id="msg_per_id_error">Must
						be numbers only</div>
					<p id="validate_msg" class="alert toggle btn">You must correct the errors first.</p>
				</div>
				<input type="submit" value="Next">
			</form>
			<p>Help:</p>
			<p>
				<a href="#wimu" data-role="button" data-rel="dialog" data-transition="pop">What's my Username?</a>
				<a href="#wmid" data-role="button" data-rel="dialog" data-transition="pop">What's my ID?</a>
			</p>
		</div>
		<div data-theme="b" data-role="footer" data-position="fixed">
			<h3>CFCC</h3>
		</div>
	</div>
	<jsp:include page="m_dialog.jsp" />
</body>
</html>