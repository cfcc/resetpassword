<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet" href="css/jquery.mobile-1.3.2.min.css" />
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery.mobile-1.3.2.min.js"></script>
<title><%=application.getAttribute("title")%></title>
</head>
<body>
<!-- Home -->
<div data-role="page" id="page1">
    <div data-theme="b" data-role="header">
        <h3>
            Reset Password
        </h3>
    </div>
    <div data-role="content">
        <p>Please choose a new password, it must be at least <%=session.getAttribute("passwd_min_len")%> characters in length, must must have a mix of upper and lower case letters, and have at least one number.</p>
        <p>Please note that passwords cannot include your any part of your name.</p>
		<%
			String msg = (String) session.getAttribute("password_result");
			if (msg != null) {
		%>
		<div class="alert">
			<p>
				<b>Password is not valid:</b>
			</p>
			<%=msg%>
		</div>
		<%
			}
		%>
		<form id="simpleform" autocomplete="off" action="<%=response.encodeURL("ResetPassword")%>" method="post">
			<input type="hidden" name="step" value="change">
               <input type="hidden" name="txt_per_id" value="<%=session.getAttribute("person_id")%>">
               <label for="txt_passwd">New Password:</label>
               <input type="password" name="txt_passwd" id="txt_passwd" size="30" class="text ui-widget-content ui-corner-all" />
               <label for="txt_passwd_check">Confirm Password:</label>
               <input type="password" name="txt_passwd_check" id="txt_passwd_check" size="30" class="text ui-widget-content ui-corner-all" /><br>
               <select name="sel_question" id="sel_question" onchange="clearField('txt_secret')">
			<%
				String[] questions = (String[]) session.getAttribute("security_questions");
				String curr_question = (String) session.getAttribute("curr_question");
				for (int i = 0; i < questions.length; i++) {
					if (questions[i].equals(curr_question)) {
						out.println("<option selected>" + questions[i] + "</option>");
					} else {
						out.println("<option>" + questions[i] + "</option>");
					}
				}
			%>
			</select>
               <input type="text" name="txt_secret" id="txt_secret" size="25" maxlength="<%=session.getAttribute("secret_len")%>" value="<%=session.getAttribute("secret")%>" />
               <input type="submit" name="cmd_change_passwd" id="cmd_change_passwd" value="Finished">
		</form>
    </div>
    <div data-theme="b" data-role="footer" data-position="fixed">
        <h3>
            CFCC
        </h3>
    </div>
</div>
</body>
</html>
