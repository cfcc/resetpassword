<!-- Dialog template included in m_begin.jsp -->
<div data-role="page" id="wimu">
	<div data-role="header" data-theme="b">
		<h1>What's my Username?</h1>
	</div>
	<div data-role="content" data-theme="b">
		<form action="/ResetPassword/UserName" method="post">
		<input type="hidden" name="asHtml" value="true" />
			<fieldset>
				<p>
					<label for="per_last" class="ui-hidden-accessible">Last Name<sup>*</sup></label>
					<input type="text" name="per_last" id="per_last" placeholder="Last Name*" value="" />
				</p>
				<hr>
				<p>
					<label for="per_id" class="ui-hidden-accessible">CFCC ID Number</label>
					<input type="text" name="per_id" id="per_id" placeholder="ID Number" value="" maxlength="7" />
				</p>
				<p>OR</p>
				<p>
					<label for="per_ssn" class="ui-hidden-accessible">Social Security Number</label>
					<input type="text" name="per_ssn" id="per_ssn" placeholder="Social Security Number" value="" maxlength="11" />
				</p>
				<p>
					<sup>* = Required</sup>
				</p>
			</fieldset>
			<a href="page1" data-rel="back" data-role="button" data-inline="true" data-icon="back">Back</a>
			<input type="submit" value="Next" data-inline="true" data-icon="plus" />
		</form>
	</div>
	<div data-role="footer">
	<h4>CFCC</h4>
	</div>
</div>

<div data-role="dialog" id="wmid">
	<div data-role="header" data-theme="b">
		<h1>What's My ID?</h1>
	</div>
	<div data-role="content" data-theme="b">
		<p>ID numbers are not given out	over the phone or in email.  You must visit the appropriate office in person.</p>
		<p>CFCC students and employees contact the Help Desk: 910 362-4357</p>
		<p>NC Wesleyan students contact the extension office: 910 520-6786</p>
		<a href="page1" data-rel="back" data-role="button" data-icon="back">Back</a>
	</div>
	<div data-role="footer">
		<h4>CFCC</h4>
	</div>
</div>