<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet" href="css/jquery.mobile-1.3.2.min.css" />
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery.mobile-1.3.2.min.js"></script>
<title><%=application.getAttribute("title")%></title>
</head>
<body>
<!-- Home -->
<div data-role="page" id="page1">
    <div data-theme="b" data-role="header">
        <h3>
            Reset Password
        </h3>
    </div>
    <div data-role="content">
    	<h2>Error!</h2>
		<p>
		<%=(String) application.getAttribute("error_msg")%>
		</p>
		<%
			Boolean debug = (Boolean) application.getAttribute("debug");
			if (debug.booleanValue() == true) {
				String log_msg = (String) application.getAttribute("log_msg");
				out.append("<p class=\"debug\">").append(log_msg)
						.append("</p>\n");
			}
		%>
		<a data-role="button" href="<%=(String) application.getAttribute("link")%>" data-ajax="false">
		Start over...
		</a>
    </div>
    <div data-theme="b" data-role="footer" data-position="fixed">
        <h3>
            CFCC
        </h3>
    </div>
</div>
</body>
</html>
