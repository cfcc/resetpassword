<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet" href="css/jquery.mobile-1.3.2.min.css" />
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery.mobile-1.3.2.min.js"></script>
<title><%=application.getAttribute("title")%></title>
</head>
<body>
<!-- Home -->
<div data-role="page" id="page1">
    <div data-theme="b" data-role="header">
        <h3>
            Reset Password
        </h3>
    </div>
    <div data-role="content">
		<h2>Success</h2>

		<p>Your password in the Directory Server was updated.</p>

		<%
			String warning_msg = (String) application.getAttribute("warning");
			if (warning_msg != null) {
				out.print("<div class=\"warning\">" + warning_msg + "</div>\n");
			}
		%>
		<a data-role="button" href="<%=application.getAttribute("server_url")%>/">
			Continue to <%=application.getAttribute("server_name")%>...
		</a>
    </div>
    <div data-theme="b" data-role="footer" data-position="fixed">
        <h3>
            CFCC
        </h3>
    </div>
</div>
</body>
</html>
