<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>What's My Username</title>
</head>
<body>
	<div data-role="page" id="username" data-theme="b">
		<div data-role="header" data-theme="b">
			<h1>What's My Username?</h1>
		</div>
		<div data-role="content">
			<% 
			Boolean success = (Boolean) application.getAttribute("success"); 
			if (success.booleanValue() == true) { 
			%>
				<p>Your Username is:</p>
				<p><strong><%=application.getAttribute("username")%></strong></p>
			<% } else { %>
				<p>With the information you provided we could not uniquely identify you. Please check the data you entered.</p>
			<% } %>
			<p><a href="<%=application.getAttribute("link")%>" data-role="button">Done</a>
		</div>
		<div data-role="footer" data-theme="b">
			<h4>CFCC</h4>
		</div>
	</div>
</body>
</html>