<%@page import="java.util.ArrayList"%>
<%@page import="org.cfcc.servlet.LocalConfigItem"%>
<%@page import="java.util.Iterator"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	ArrayList<LocalConfigItem> field_list = (ArrayList<LocalConfigItem>) application.getAttribute("field_list");
	String first_field = field_list.get(0).getName();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet" href="css/jquery.mobile-1.3.2.min.css" />
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery.mobile-1.3.2.min.js"></script>
<link type="text/css" href="css/basic.css" rel="Stylesheet" media="all">
<script type="text/javascript" src="js/validate.js"></script>
<title><%=application.getAttribute("title")%></title>
</head>
<body>
<div data-role="page" id="page2">
    <div data-theme="b" data-role="header">
        <h3>
            Reset Password
        </h3>
    </div>
    <div data-role="content">
	<form id="simpleform" autocomplete="off" data-ajax="false"
	      action="<%=response.encodeURL("ResetPassword")%>" method="post"
	      onsubmit="return checkValidationForm();">
	  <input type="hidden" name="step" value="validate">
	  <input type="hidden" name="txt_username" value="<%=session.getAttribute("username")%>">
	  <input type="hidden" name="txt_per_id" value="<%=session.getAttribute("person_id")%>">
<!-- Verification Questions -->
	  <%
	  for (LocalConfigItem field : field_list) {
		  %>
			<label for="txt_<%=field.getName()%>"><%=field.getDescription()%></label>
		  	<input type="text" name="txt_<%=field.getName() %>" size="<%=field.getLength() + 2 %>" 
		    	   maxlength="<%=field.getLength() %>" id="txt_<%=field.getName() %>"
		    	   placeholder="<%=field.getExampleSimple() %>"
				   onBlur="check_<%=field.getName() %>(this)" />
			<div class="alert toggle" id="msg_<%=field.getName() %>_error"><%=field.getError() %></div>
	      <%
	  }
	      %>
<!-- Challenge Question -->
	      <%
	      	String curr_question = (String) session
	      			.getAttribute("curr_question");
	      	if (!curr_question.equals("")) {
	      		String curr_example = (String) session
	      				.getAttribute("curr_example");
	      %>
		<label for="txt_secret"><%=curr_question%>?</label>
		<input type="text" name="txt_secret" size="20" maxlength="35" id="txt_secret" />
	      <%
	      	} // end-if curr_question != ""
	      %>
		<p id="validate_msg" class="alert toggle btn">You must correct the errors first.</p>
	    <input type="submit" name="cmd_submit" value="Next">
	</form>
    </div>
    <div data-theme="b" data-role="footer" data-position="fixed">
        <h3>
            CFCC
        </h3>
    </div>
</div>
</body>
</html>
