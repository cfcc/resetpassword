<%@page import="java.util.ArrayList"%>
<%@page import="org.cfcc.servlet.LocalConfigItem"%>
<%@page import="java.util.Iterator"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<title><%=application.getAttribute("title")%></title>
<link type="text/css" href="css/redmond/jquery-ui-1.10.3.custom.css"
	rel="Stylesheet" />
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<link type="text/css" href="css/basic.css" rel="Stylesheet" media="all">
<link type="text/css" href="css/theme.css" rel="Stylesheet" media="all">
<%
	if (application.getAttribute("css_url") != null) {
		out.print("<link type=\"text/css\" href=\""
				+ application.getAttribute("css_url")
				+ "\" rel=\"Stylesheet\" media=\"all\">");
	}
	ArrayList<LocalConfigItem> field_list = (ArrayList<LocalConfigItem>) application.getAttribute("field_list");
	String first_field = field_list.get(0).getName();
%>
<script type="text/javascript" src="js/validate.js"></script>
<script type="text/javascript">
	$(function() {
		$("input:submit", "#simpleform").button();
	});
</script>
</head>
<body onload="setFocus('txt_<%=first_field %>')">
    <div id="page">
      <jsp:include page="header.html" />
      <div id="content">
	<h1>Validate Your Request</h1>

	<jsp:include page="help_for_validation.html"/>

	<form id="simpleform" autocomplete="off"
	      action="<%=response.encodeURL("ResetPassword")%>" method="post"
	      onsubmit="return checkValidationForm();">
	  <input type="hidden" name="step" value="validate">
	  <input type="hidden" name="txt_username" value="<%=session.getAttribute("username")%>">
	  <input type="hidden" name="txt_per_id" value="<%=session.getAttribute("person_id")%>">
	  <fieldset>
<!-- Verification Questions -->
	  <%
	  for (LocalConfigItem field : field_list) {
		  %>
	    <div class="questionBox">
	      <div class="questionBoxLabel">
			<label for="txt_<%=field.getName()%>"><%=field.getDescription()%></label>
	      </div>
	      <div class="questionBoxInput">
		  	<input type="text" name="txt_<%=field.getName() %>" size="<%=field.getLength() + 2 %>" 
		    	   maxlength="<%=field.getLength() %>" id="txt_<%=field.getName() %>"
				   onBlur="check_<%=field.getName() %>(this)"
		           class="text ui-widget-content ui-corner-all" />
			<i><%=field.getExample() %></i>
			<div class="ui-state-error toggle" id="msg_<%=field.getName() %>_error"><%=field.getError() %></div>
	      </div>
	    </div>
	      <%
	  }
	      %>
<!-- Challenge Question -->
	      <%
	      	String curr_question = (String) session
	      			.getAttribute("curr_question");
	      	if (!curr_question.equals("")) {
	      		String curr_example = (String) session
	      				.getAttribute("curr_example");
	      %>
	      <div class="questionBox">
	      <div class="questionBoxLabel">
		<label for="txt_secret"><%=curr_question%>?</label>
	      </div>
	      <div class="questionBoxInput">
		<input type="text" name="txt_secret" size="20" maxlength="35"
		       id="txt_secret"
		       class="text ui-widget-content ui-corner-all" />
		<i>ex: <%=curr_example%></i>
	      </div>
	    </div>
	      <%
	      	} // end-if curr_question != ""
	      %>
	  </fieldset>
	  <p id="validate_msg" class="alert toggle btn">You must correct the errors first.</p>
	  <p class="btn">
	    <input type="submit" name="cmd_submit" value="Next">
	  </p>
	</form>
	<p class="copyright"><%=application.getAttribute("version")%></p>
      </div>
      <jsp:include page="footer.html" />
    </div>
  </body>
</html>
