#!/usr/bin/env python3
"""Update the project version number in an XML file.

Written to automatically update the version string in the web.xml file of the
ResetPassword project.  It is looking for a particular init parameter that is
named version and then it updates the xml.  For example:

        <init-param>
            <param-name>version</param-name>
            <param-value>4.1.0rc1</param-value>
        </init-param>

If you use the -p option you can specify an alternate param name, ex: build.

This is a python version of the original perl script in the ldap project.

"""
from optparse import OptionParser
import re
import datetime
import subprocess

DEBUG = False
QUIET = False

def main():
    """Parse the command line options and run the process."""
    global DEBUG
    global QUIET
    
    parser = OptionParser(usage="%prog [options] TARGET_FILE_NAME")
    parser.add_option("-c", "--code", dest="version_code",
                      default="", metavar="NAME",
                      help="Specify an a version code (Ex: test, rc1)")
    parser.add_option("-d", "--debug", dest="debug",
                      action="store_true", default=False,
                      help="Display debugging messages")
    parser.add_option("-g", "--git", dest="use_git",
                      action="store_true", default=False,
                      help="Use the latest Git tag as the version number")
    parser.add_option("-m", "--mercurial", dest="use_mercurial",
                      action="store_true", default=False,
                      help="Use the latest Mercurial tag as the version number")
    parser.add_option("-p", "--parameter", dest="param_name",
                      default="version", metavar="NAME",
                      help="Specify a parameter name (default: version)")
    parser.add_option("-q", "--quiet", dest="quiet",
                      action="store_true", default=False,
                      help="Suppress printing of status messages.")
    
    (options, args) = parser.parse_args()

    DEBUG = options.debug
    QUIET = options.quiet

    if len(args) < 1:
        parser.error("You must specify a target file to update.")
        
    target_fn = args[0]
    
    re_param_name = re.compile("\<param-name\>(.*?)\<\/param-name\>")
    re_param_value = re.compile("\<param-value\>(\d+)-(\d+)\<\/param-value\>")
    re_param_any_value = re.compile("\<param-value\>(.*?)\<\/param-value\>")
    
    new_date = datetime.date.today().strftime("%Y%m%d")
    new_build = 1
    if options.version_code != "":
        version_code = "-" + options.version_code
    else:
        version_code = ""
        
    if options.use_mercurial:
        result = subprocess.check_output(['hg', 'log', '-r .', '--template={latesttag}\n'])
        version_name = result.decode(encoding='UTF-8').split("-")[1]
    if options.use_git:
        result = subprocess.check_output(['git', 'describe', '--tags', '--abbrev=0'])
        version_name = result.decode(encoding='UTF-8').split("-")[1]
    
    content_out = ""
    
    param_name = ""
    with open(target_fn, 'r') as fd:
        for line in fd:
            new_line = None
            result = re_param_name.search(line)
            if result is not None:
                param_name = result.group(1)
                if not QUIET:
                    print("[INFO] Found parameter:", param_name)
            if param_name == options.param_name:
                print("[INFO] Parameter is the one we're looking for...")
                if options.use_mercurial:
                    result = re_param_any_value.search(line)
                    if result is not None:
                        new_line = "                       <param-value>%s</param-value>\n" % (version_name)
                else:
                    result = re_param_value.search(line)
                    if result is not None:
                        old_date = result.group(1)
                        try:
                            old_build = int(result.group(2))
                        except ValueError:
                            old_build = 0
                        if not QUIET:
                            print("[INFO] Found param value:", old_date, old_build)
                        if old_date == new_date:
                            new_build = old_build + 1
                        # note the spaces in the next line are to preserve the XML formatting
                        new_line = "                       <param-value>%s-%d%s</param-value>\n" % (new_date, new_build, version_code)
            if new_line is None:
                content_out += line
            else:
                content_out += new_line

    if DEBUG:
        print(content_out)
    else:
        with open(target_fn, 'w') as fd:
            fd.write(content_out)

def getMercurialTag():
    """Apply the mercurial tag as teh version number.

    This requires that the nearest extension be installed locally.

    http://mercurial.selenic.com/wiki/NearestExtension
    
    """
    # Running ...
    #
    #     hg nearest -1 | awk ' { print $3 } '
    #
    # ... will give you the most recent tag which then can be applied as a
    # version number in the XML file.
            
if __name__ == "__main__":
    main()
