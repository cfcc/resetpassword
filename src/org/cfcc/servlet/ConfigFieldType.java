package org.cfcc.servlet;

public enum ConfigFieldType {
	INVALID, TITLE, IDENTITY, VERIFY, CHALLENGE
}
