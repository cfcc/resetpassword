package org.cfcc.servlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

public class LocalConfig {
	HashMap<String, String> schoolTitle;
	HashMap<String, ArrayList<LocalConfigItem>> schoolFields;
	
	private static final String[] IDENTITY_FIELDS = {"username", "person_id"};
	private Scanner verif_tok; 
	
	public LocalConfig() {}
	
	public LocalConfig(Properties props) {
		schoolTitle = new HashMap<String, String>();
		schoolFields = new HashMap<String, ArrayList<LocalConfigItem>>();
		Scanner tokenize = null;
		if (props.containsKey("schools")) {
			try {
				tokenize = new Scanner(props.getProperty("schools"));
				tokenize.useDelimiter(",\\s*");
				while (tokenize.hasNext()) {
					String school_code = tokenize.next();
					// save the school's title
					String k = school_code + ".title";
					if (props.containsKey(k)) {
						schoolTitle.put(school_code, props.getProperty(k));
					}
					// Create a new list to save the fields we find in the properties file
					ArrayList<LocalConfigItem> school_fields = new ArrayList<LocalConfigItem>();
					// the first page will always have a user name and a numeric ID
					k = school_code + ".identity.source";
					String ident_source = props.getProperty(k);
					for (int i=0; i<IDENTITY_FIELDS.length; i++) {
						LocalConfigItem item = new LocalConfigItem(ConfigFieldType.IDENTITY);
						item.setName(IDENTITY_FIELDS[i]);
						item.setSource(ident_source);
						k = school_code + ".identity." + IDENTITY_FIELDS[i] + ".description";
						if (props.containsKey(k)) {
							item.setDescription(props.getProperty(k));
						}
						k = school_code + ".identity." + IDENTITY_FIELDS[i] + ".length";
						if (props.containsKey(k)) {
							item.setLength(Integer.parseInt(props.getProperty(k))); 
						}
						school_fields.add(item);
					}
					// the verification page questions are configurable, so we'll find the field
					// that gives their names
					k = school_code + ".verify";
					if (props.containsKey(k)) {
						verif_tok = new Scanner(props.getProperty(k));
						verif_tok.useDelimiter(",\\s*");
						while (verif_tok.hasNext()) {
							String field_name = verif_tok.next();
							LocalConfigItem item = new LocalConfigItem(ConfigFieldType.VERIFY);
							item.setName(field_name);
							String prefix = school_code + ".verify." + field_name;
							// Store the common fields first
							k = prefix + ".description";
							if (props.containsKey(k)) {
								item.setDescription(props.getProperty(k));
							}
							k = prefix + ".example";
							if (props.containsKey(k)) {
								item.setExample(props.getProperty(k));
							}
							k = prefix + ".example_simple";
							if (props.containsKey(k)) {
								item.setExampleSimple(props.getProperty(k));
							}
							k = prefix + ".error";
							if (props.containsKey(k)) {
								item.setError(props.getProperty(k));
							}
							k = prefix + ".length";
							if (props.containsKey(k)) {
								int length = Integer.parseInt(props.getProperty(k));
								item.setLength(length);
							}
							k = prefix + ".conversion";
							if (props.containsKey(k)) {
								item.setConv(props.getProperty(k));
							}
							k = prefix + ".numbers_only";
							if (props.containsKey(k)) {
								boolean value = Boolean.parseBoolean(props.getProperty(k));
								item.setNumeric(value);
							}
							// find out what kind of field this is to load type-specific variables
							k = prefix + ".source";
							if (props.containsKey(k)) {
								String field_source = props.getProperty(k);
								item.setSource(field_source);
								if (field_source.equals("ldap")) {
									// get the LDAP information
									k = prefix + ".attribute";
									if (props.containsKey(k)) {
										item.setAttribute(props.getProperty(k));
									}
									k = prefix + ".encrypted";
									if (props.containsKey(k)) {
										boolean encrypted = Boolean.parseBoolean(props.getProperty(k));
										item.setEncrypted(encrypted);
									}
								} else {
									if (field_source.equals("unidata")) {
										// get the UniData information
										k = prefix + ".file";
										if (props.containsKey(k)) {
											item.setTable(props.getProperty(k));
										}
										k = prefix + ".location";
										if (props.containsKey(k)) {
											int loc = Integer.parseInt(props.getProperty(k));
											item.setLocation(loc);
										}
									} else {
										if (field_source.equals("unidata foreign key")) {
											// get the unidata fk information
											k = prefix + ".file";
											if (props.containsKey(k)) {
												item.setTable(props.getProperty(k));
											}
											k = prefix + ".location";
											if (props.containsKey(k)) {
												int loc = Integer.parseInt(props.getProperty(k));
												item.setLocation(loc);
											}
											k = prefix + ".remote_file";
											if (props.containsKey(k)) {
												item.setRemoteTable(props.getProperty(k));
											}
											k = prefix + ".remote_location";
											if (props.containsKey(k)) {
												int loc = Integer.parseInt(props.getProperty(k));
												item.setRemoteLocation(loc);
											}
										}
									}
								}
							}
							school_fields.add(item);
						}
					}
					// there is a challenge question and answer that is either stored in UniData or LDAP
					k = school_code + ".challenge.source";
					if (props.containsKey(k)) {
						LocalConfigItem item = new LocalConfigItem(ConfigFieldType.CHALLENGE);
						String challenge_source = props.getProperty(k);
						item.setSource(challenge_source);
						if (challenge_source.equals("unidata")) {
							k = school_code + ".challenge.file";
							if (props.containsKey(k)) {
								item.setTable(props.getProperty(k));
							}
							k = school_code + ".challenge.question.location";
							if (props.containsKey(k)) {
								int loc = Integer.parseInt(props.getProperty(k));
								item.setLocation(loc);
							}
							k = school_code + ".challenge.answer.location";
							if (props.containsKey(k)) {
								int loc = Integer.parseInt(props.getProperty(k));
								item.setRemoteLocation(loc);
							}
						} else {
							if (challenge_source.equals("ldap")) {
								k = school_code + ".challenge.attribute";
								if (props.containsKey(k)) {
									item.setAttribute(props.getProperty(k));
								}
							}
						}
						if (item != null) {
							school_fields.add(item);
						}
					}
					schoolFields.put(school_code, school_fields);
				}
			} finally {
				if (tokenize != null) {
					tokenize.close();
				}
			}
		}
	}
	
	public HashMap<String, String> getSchoolTitle() {
		return schoolTitle;
	}
	
	public HashMap<String, ArrayList<LocalConfigItem>> getSchoolFields() {
		return schoolFields;
	}
	
	public Set<String> getSchoolCodes() {
		Set<String> keys = schoolFields.keySet();
		return keys;
	}
	
	public ArrayList<LocalConfigItem> getFields(String school, ConfigFieldType ofType) {
		ArrayList<LocalConfigItem> fields_out = new ArrayList<LocalConfigItem>();
		if (schoolFields.containsKey(school)) {
			ArrayList<LocalConfigItem> field_list = schoolFields.get(school);
			for (LocalConfigItem lci : field_list) {
				if (lci.getType() == ofType) {
					fields_out.add(lci);
				}
			}
		}
		return fields_out;
	}

	public String getIdentitySource(String school_code) {
		String result = null;
		ArrayList<LocalConfigItem> fields = getFields(school_code, ConfigFieldType.IDENTITY);
		if (!fields.isEmpty()) {
			LocalConfigItem item = fields.get(0);
			result = item.getSource();
		}
		return result;
	}
	
	public String getSchoolInfoAsJS() {
		StringBuffer result = new StringBuffer("school_info = {\n");
		for (String school_code : getSchoolCodes()) {
			ArrayList<LocalConfigItem> fields = getFields(school_code, ConfigFieldType.IDENTITY);
			result.append("'").append(school_code).append("': { ");
			for (LocalConfigItem item : fields) {
				result.append("'").append(item.getName());
				result.append("': { 'description': '").append(item.getDescription()).append("', ");
				result.append("'length': ").append(item.getLength()).append(" }, ");
			}
			result.append("},\n");
		}
		result.append("\n};");
		return result.toString();
	}
}
