package org.cfcc.servlet;

public class LocalConfigItem {
	private String name = null;
	private ConfigFieldType type = null;
	private String description = null;
	private String example = null;
	private String example_simple = null;
	private String error = null;
	private int length = -1;
	private String conversion_code = "";
	private String source = null;
	private String table = null;
	private int location = -1;
	private String remote_table = null;
	private int remote_location = -1;
	private String attribute = null;
	private boolean encrypted = false;
	private boolean numeric = false;
	
	public LocalConfigItem() {
		type = ConfigFieldType.INVALID;
	}
	
	public LocalConfigItem(ConfigFieldType value) {
		type = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String value) {
		name = value;
	}

	public ConfigFieldType getType() {
		return type;
	}

	public void setType(ConfigFieldType type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExample() {
		return example;
	}

	public void setExample(String example) {
		this.example = example;
	}

	public String getExampleSimple() {
		return example_simple;
	}

	public void setExampleSimple(String example_simple) {
		this.example_simple = example_simple;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public String getRemoteTable() {
		return remote_table;
	}

	public void setRemoteTable(String remote_table) {
		this.remote_table = remote_table;
	}

	public int getRemoteLocation() {
		return remote_location;
	}

	public void setRemoteLocation(int remote_location) {
		this.remote_location = remote_location;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public boolean isEncrypted() {
		return encrypted;
	}

	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}

	public boolean isNumeric() {
		return numeric;
	}

	public void setNumeric(boolean numbers_only) {
		this.numeric = numbers_only;
	}
	
	public void setConv(String conv_code) {
		this.conversion_code = conv_code;
	}

	public String getConv() {
		return conversion_code;
	}

}
