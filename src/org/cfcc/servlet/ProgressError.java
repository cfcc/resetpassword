/*
 * Copyright 2012 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.cfcc.servlet;

/**
 * Custom exception class that is thrown by the servlet to log errors that 
 * happen while processing the user's request.
 * 
 * @author jfriant
 *
 */
public class ProgressError extends Exception {

	/** Automatically generated ID */
	private static final long serialVersionUID = 4022387916620689451L;
	/** The error code */
	private ProgressErrorType error_code; 
	/** Holds the current Colleague Person ID */ 
	private String person_id = "";
	/** Additional information that is not part of error message */
	private String addtl_msg = "";
	/** The original exception, saved for later traceback report */
	public Throwable orig = null;
	
	/** default constructor */
	public ProgressError(String s) {
		super(s);
		error_code = ProgressErrorType.MISMATCH;
	}

	/** Single message with a error type code */
	public ProgressError(String s, ProgressErrorType code) {
		super(s);
		error_code = code;
	}
	
	/**
	 * This constructor sets a custom error message and the Person ID
	 */
	public ProgressError(String message, String person_id) {
		super(message);
		this.person_id = person_id;
		error_code = ProgressErrorType.MISMATCH;
	}

	public ProgressError(String message, String person_id, ProgressErrorType code) {
		super(message);
		this.person_id = person_id;
		error_code = code;
	}

	/** Include the original exception */
    public ProgressError(String message, Throwable orig) {
        super(message);
        this.orig = orig;
		error_code = ProgressErrorType.MISMATCH;
    }

    public ProgressError(String message, Throwable orig, ProgressErrorType code) {
        super(message);
        this.orig = orig;
		error_code = code;
    }

    public ProgressError(String message, String person_id, Throwable orig) {
        super(message);
        this.person_id = person_id;
        this.orig = orig;
		error_code = ProgressErrorType.MISMATCH;
    }

    /** Include the original exception and specify the error code*/
    public ProgressError(String message, String person_id, Throwable orig, ProgressErrorType code) {
        super(message);
        this.person_id = person_id;
        this.orig = orig;
		error_code = code;
    }

	public ProgressError(String message, String person_id, String addtl_msg) {
		super(message);
		this.person_id = person_id;
		this.addtl_msg = addtl_msg;
		error_code = ProgressErrorType.MISMATCH;
	}

	public ProgressError(String message, String person_id, String addtl_msg, ProgressErrorType code) {
		super(message);
		this.person_id = person_id;
		this.addtl_msg = addtl_msg;
		error_code = code;
	}
    
    public ProgressErrorType getErrorType() {
    	return error_code;
    }
    
    /**
     * Returns a detailed error message. 
     */
    @Override
    public String toString() {
    	StringBuffer m = new StringBuffer("[ProgressError] ");
    	m.append(super.getMessage());
    	m.append(" (track_id=").append(person_id).append(")");
    	if (this.orig != null) {
    	    m.append(" Caused by ").append(this.orig.toString());
        }
        if (!addtl_msg.equals("")) {
            m.append(", ").append(addtl_msg);
        }
        return m.toString();

    }
}
