/*
 * Copyright 2012 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.cfcc.servlet;

/**
 * @author jfriant
 * 
 */
public enum ProgressErrorType {
	MISMATCH, LOCKED, 
	SESSION_TIMEOUT, SESSION_INVALID,
	FAILURE,
	INVALID_SECRET, SECRET_FAILED,
	BAD_PASSWORD, PASSWORD_HISTORY,
	SYSTEM
}
