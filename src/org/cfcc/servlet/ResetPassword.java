/*
 * Copyright 2007-2012 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.cfcc.servlet;

import asjava.uniclientlibs.UniException;
import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSessionException;

import java.io.*;
import java.security.Security;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.acegisecurity.providers.ldap.LdapShaPasswordEncoder;
import org.cfcc.*;
import org.cfcc.colleague.Environment;
import org.cfcc.colleague.OrgEntityEnv;
//import org.cfcc.services.GaUpdate;
import org.cfcc.services.Ldap;
import org.cfcc.utils.ChallengeData;
import org.cfcc.utils.Validate;

import com.handinteractive.mobile.UAgentInfo;

/**
 * Main class for this project, servlet to display the reset password pages.
 * 
 * @author Jakim Friant
 */

public class ResetPassword extends HttpServlet {

	/** Auto-Generated version ID */
	private static final long serialVersionUID = -3817165802143728543L;
	/** Local configuration that holds the connection information. */
	private ServletConfig lcfg;
	/** Local properties file with connection information */
	private Properties properties;
	/** Default properties for the Servlet configuration */
	private Properties default_props;
	/** Link to the validation question page */
	private final String VLD_PG = "validate_request.jsp";
	/** Link to the change password page */
	private final String CHG_PG = "change_password.jsp";
	/** Link to the main index page if needed */
	private final String IDX_PG = "begin.jsp";
	/** Link to the success/finished page */
	private final String FIN_PG = "finished.jsp";
	/** Link to the error page */
	private final String ERR_PG = "error.jsp";
	/**
	 * Is the LDAP server Active Directory, which requires a special password.
	 * Set in ResetPassword.properties.
	 */
	private boolean use_unicode_password = false;
	/**
	 * Should we show error messages that normally only go to the logs? Set in
	 * ResetPassword.properties
	 */
	private boolean debug = false;
	/** A custom file that stores the security questions and answers */
	private String local_params_filename = "";
	/**
	 * General error message to display, these can be changed in the properties
	 * file
	 */
    private Map<ProgressErrorType, String> m_errors = Collections
			.synchronizedMap(new EnumMap<ProgressErrorType, String>(
					ProgressErrorType.class));
	private static final Logger servlet_log = Logger
			.getLogger(ResetPassword.class.getName());
	/** Validator is used to check passwords */
	private Validate validator;
	/**
	 * Use password salt is to configure OpenLDAP passwords (SHA1 or SSHA1),
	 * disable for Google Apps
	 */
	private boolean use_password_salt;
	private LocalConfig school_config;

	private final String mutex = "";

	/**
	 * The init function take the configuration and calls the parent class to
	 * build the servlet.
	 * 
	 * @param config
	 *            the configuration object that is saved for later reference.
	 * @throws javax.servlet.ServletException
	 *             If the servlet fails to initialize for any reason.
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		lcfg = config;
		ServletContext cntx = getServletContext();
		cntx.setAttribute("version", lcfg.getInitParameter("version"));
		cntx.setAttribute("build", lcfg.getInitParameter("build"));
		cntx.setAttribute("copyright_year",
				lcfg.getInitParameter("copyright_year"));
		cntx.setAttribute("title", lcfg.getInitParameter("title"));
		servlet_log.setLevel(Level.FINE);

		String keystore_path = lcfg.getInitParameter("keystore_path");
		if (keystore_path != null) {
			/* set up access to an alternate keystore */
			Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
			System.setProperty("javax.net.ssl.trustStore", keystore_path);
			/* Using the default keystore password */
			System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
		}

		// Load the Servlet configuration from the default properties file
		try {
			loadDefaultProperties();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Load the School-specific fields from a local properties file
		try {
			loadSchoolProperties();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Default properties are compiled into the WAR file and only loaded when
	 * the servlet is deployed.
	 */
    private void loadDefaultProperties() throws IOException {
		synchronized (mutex) {
			ServletContext cntx = getServletContext();

			// create and load default properties
			default_props = new Properties();
			default_props.load(cntx.getResourceAsStream("/WEB-INF/default.properties"));

			// Assign the error messages to the message type
			for (ProgressErrorType p : ProgressErrorType.values()) {
				String k = "servlet.m." + p.name().toLowerCase();
				m_errors.put(p, default_props.getProperty(k));
			}
		}
	}

    /*
     * Connection properties are loaded from a file on the server.
     * The file path is set in the default.properties file and loaded
     * by init().
     */
    private void loadProperties() throws FileNotFoundException, IOException {
		// load the properties file to get the UniData and LDAP connection
		// passwords, if this fails it will throw an IOException and terminate
		synchronized (mutex) {
			ServletContext cntx = getServletContext();

			// create application properties with defaults
			properties = new Properties(default_props);

			// load local properties from the system
			String prop_path = default_props.getProperty("properties_path");
			properties.load(new FileInputStream(prop_path));

			// Check for a debug flag, should default to false
			if (properties.getProperty("servlet.debug", "false").equals("true")) {
				debug = true;
				servlet_log.setLevel(Level.CONFIG);
			} else {
				debug = false;
			}
			// check for a Unicode password flag, should default to false
            use_unicode_password = properties.getProperty("ldap.use_unicode_password", "false")
                    .equals("true");
			// check for the password salt setting (OpenLDAP only)
            use_password_salt = properties.getProperty("ldap.use_password_salt", "false").equals("true");
			// Look for an optional CSS link, if it's not there, make sure it
			// is not in the Servlet context either
			if (properties.containsKey("servlet.css.url")) {
				cntx.setAttribute("css_url",
						properties.getProperty("servlet.css.url"));
			} else {
				cntx.removeAttribute("css_url");
			}
			// Store the link for the finished page
			cntx.setAttribute("server_url",
					properties.getProperty("finished.server_url"));
			cntx.setAttribute("server_name",
					properties.getProperty("finished.server_name"));
			// Look for a table name for the parameters, if it is blank then
			// we will be using the PERSON file
			if (properties.containsKey("db.local_params")) {
				local_params_filename = properties.getProperty("db.local_params");
			} else {
				local_params_filename = "";
			}
            // Re-assign the error messages to the message type to pick up any changes
            for (ProgressErrorType p : ProgressErrorType.values()) {
                String k = "servlet.m." + p.name().toLowerCase();
                m_errors.put(p, properties.getProperty(k));
            }
            // Initialize the validation object for later use
			validator = new Validate(properties);
		}
	}
	
	/**
	 * Each school can specify what questions are asked and where the data is stored.
	 * @throws IOException If the properties file cannot be located.
	 */
    private void loadSchoolProperties() throws IOException {
		synchronized (mutex) {
			ServletContext cntx = getServletContext();
			
			Properties school_props = new Properties();
			school_props.load(cntx.getResourceAsStream("/WEB-INF/school.properties"));
			school_config = new LocalConfig(school_props);
		}
	}

	/**
	 * The function called by a GET request, returns the index page.
	 * 
	 * @param req
	 *            the HTTP request object
	 * @param res
	 *            the HTTP response object
	 * @throws IOException
	 *             If the index page cannot be found.
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		res.setContentType("text/html");
		res.setHeader("Pragma", "no-cache");
		res.setDateHeader("Expires", 0);

		// Load the Servlet configuration from the system properties file
		try {
			loadProperties();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Boolean is_passwordhelp = Validate.checkForParam(req.getParameterNames(), "passwordhelp");
		String school_code = "";
		if (Validate.checkForParam(req.getParameterNames(), "school_code")) {
			school_code = Validate.sanitize(req.getParameter("school_code"));
		}
		ServletContext cntx = getServletContext();
		cntx.setAttribute("passwordhelp", is_passwordhelp.toString());
		cntx.setAttribute("debug", debug);
		cntx.setAttribute("environment", properties.getProperty("db.account_path"));
		cntx.setAttribute("school_titles", school_config.getSchoolTitle());
		cntx.setAttribute("multischool", "yes");
		cntx.setAttribute("school_code", school_code);
		cntx.setAttribute("school_info", school_config.getSchoolInfoAsJS());
		String next_page = IDX_PG;
		if (isThisRequestCommingFromAMobileDevice(req)) {
			next_page = "m_" + next_page;
		}
		RequestDispatcher rd = cntx.getRequestDispatcher("/" + next_page);
		if (rd != null) {
			try {
				rd.include(req, res);
			} catch (Exception e) {
				logMsg("Include problem: " + e, "", Level.SEVERE);
			}
		} else {
			res.sendError(HttpServletResponse.SC_BAD_REQUEST,
					"Page not found: " + next_page);
		}
	}

	/**
	 * Display an error page when needed.
	 */
	public void doError(PrintWriter out, String link, String msg,
			String log_msg, boolean full_page) {
		StringBuffer html = new StringBuffer("");
		if (full_page) {
			String title = lcfg.getInitParameter("title");
			html.append("<HTML>\n<HEAD><TITLE>").append(title)
					.append("</TITLE>\n");
			html.append("<link type=\"text/css\" href=\"css/basic.css\" rel=\"Stylesheet\" media=\"all\">\n");
			html.append("<link type=\"text/css\" href=\"css/start/jquery-ui-1.8.23.custom.css\" rel=\"Stylesheet\" />\n");
			html.append("<script type=\"text/javascript\" src=\"js/jquery-1.8.0.min.js\"></script>\n");
			html.append("<script type=\"text/javascript\" src=\"js/jquery-ui-1.8.23.custom.min.js\"></script>\n");
			html.append("<script type=\"text/javascript\" src=\"js/onerror.js\"></script>");
			html.append("</HEAD><BODY>\n<div id=\"content\">");
			html.append("<h2>").append(title).append("</h2>\n");
		}
		html.append("<div id='errorMessage'>\n");
		html.append("<div id='errorTitle' class=\"ui-state-highlight btn\"><b>Error!</b></div>\n");
		html.append(msg).append("\n");
		if (debug) {
			html.append("<p class=\"error\">").append(log_msg).append("</p>\n");
		}
		html.append("<p><a href=\"").append(link)
				.append("\">Start over...</a></p>");
		html.append("</div>\n");
		if (full_page) {
			html.append("</div>\n</BODY></HTML>\n");
		}
		out.print(html);
	}

	/**
	 * The function called by a POST request, process the forms and saves the
	 * new password.
	 * <p>
	 * This is the main workhorse function, it creates or verifies the HTTP
	 * session, processes the form fields, and calls the necessary functions to
	 * validate a user or password and finally to save the new password.
	 * <p>
	 * The steps for a user to reset his password are:
	 * <ol>
	 * <li>Load the index page
	 * <li>Enter a valid User Name and CFCC (person) ID on the index page (that
	 * POST is where this picks up)
	 * <li>Enter the validating information on the next page (including the
	 * SECRET if the password has been reset before)
	 * <li>The information is compared to what is in Colleague
	 * <ul>
	 * <li>If everything matches then the user can proceed
	 * <li>If one or more fields do not match then the user is presented with an
	 * error message explaining what to do, and a link to start over
	 * </ul>
	 * <li>The user is taken to the change password page to enter his password
	 * (twice for validation)
	 * <li>If no questions have been entered, then the user is prompted to
	 * select a number of validating questions and enter the answers. These will
	 * be used next time to allow the user to reset his/her password again
	 * <li>The password is saved to the LDAP database, the SECRET(s) to the
	 * UniData database, the current date to the ORG.ENTITY.ENV, and the user is
	 * presented with a success message
	 * </ol>
	 * 
	 * @param req
	 *            the HTTP request object
	 * @param res
	 *            the HTTP response object
	 * @throws ServletException
	 *             If there is a major problem with this servlet.
	 * @throws IOException
	 *             If it cannot find the change_password.html file or the
	 *             properties file.
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// boolean show_full_error_page = true;
		String next_page = IDX_PG;
		String username = null;
		String person_id = null;
		Integer progress = -1;
		String school_code = null;
		Boolean is_passwordhelp = Boolean.FALSE;

		ServletContext cntx = getServletContext();
		/* also save some page information */
		String page_info = req.getHeader("user-agent");
		String process_step = req.getParameter("step");
		
		Boolean is_mobile = isThisRequestCommingFromAMobileDevice(req);

		// Load the Servlet configuration from the system properties file
		try {
			loadProperties();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		synchronized (mutex) {

			// initialize the message buffer
			StringBuffer msg = new StringBuffer("");

			PrintWriter out = res.getWriter();

			res.setContentType("text/html");
			res.setHeader("Pragma", "no-cache");
			res.setDateHeader("Expires", 0);

			HttpSession session = req.getSession(true);

			/*
			 * TODO: I want to track how far along the session has progressed so
			 * that I can bounce people out if they get out of sequence by
			 * pressing the back button too often.
			 * 
			 * For example, each time a step is completed, the progress counter
			 * could be incremented by one. Then if we get process_step =
			 * "validate" but the counter is already on step 4, then it might be
			 * time to invalidate the session and bump them back to the start.
			 */
			progress = (Integer) session.getAttribute("progress");
			if (progress == null) {
				/*
				 * I'm making sure the person ID and username is passed along to
				 * each page
				 */
				username = Validate.sanitize(req.getParameter("txt_username")).toLowerCase();
				person_id = Validate.sanitize(req.getParameter("txt_per_id"));
				is_passwordhelp = Validate.checkForParam(
						req.getParameterNames(), "passwordhelp");
				/* The school code needs to be stored too */
				school_code = Validate.sanitize(req.getParameter("sel_school"));
				progress = 1;
				session.setAttribute("person_id", person_id);
				session.setAttribute("username", username);
				session.setAttribute("progress", progress);
				session.setAttribute("passwordhelp", is_passwordhelp.toString());
				session.setAttribute("school_code", school_code);
			} else {
				person_id = (String) session.getAttribute("person_id");
				if (person_id == null) {
					person_id = "";
				}
				username = (String) session.getAttribute("username");
				if (username == null) {
					username = "";
				}
				is_passwordhelp = Boolean.parseBoolean((String) session
						.getAttribute("passwordhelp"));
				school_code = (String) session.getAttribute("school_code");
			}

			logMsg("session progress = " + progress.toString(), person_id,
					Level.INFO);
			logMsg("school code = " + school_code, person_id, Level.INFO);

			try {
				// note: step "0" is to visit the index page and fill out the
				// form (see the doGet function above)
				switch (progress) {
				case 1:
					// step 1 is to check that the username and person id match
					// what	is in Colleague, if it is, display another page of
					// validation questions
					if (process_step.equals("start")) {
						logMsg("doPost: user agent: " + page_info
								+ " for step " + process_step, person_id,
								Level.CONFIG);
						String source = school_config
								.getIdentitySource(school_code);
						boolean found = false;
						if (source != null && source.equals("ldap")) {
							found = checkUsername(username, person_id);
						} else if (source != null && source.equals("unidata")) {
							found = checkUsernameDB(username, person_id);
						} else {
							throw new ProgressError("Configuration error with "
									+ school_code + ".identity.source",
									person_id);
						}
						if (!found) {
							throw new ProgressError("Username [" + username
									+ "] did not match person ID", person_id);
						}
						// IMPORTANT: we must check for a disabled account to
						// avoid resetting one that has been administratively locked
						try {
							boolean is_disabled = accountIsDisabled(username); 
							if (is_disabled) {
								throw new ProgressError("User account [" + username
										+ "] is disabled.", person_id,
										ProgressErrorType.LOCKED);
							}
						} catch (UidNotFoundException ex) {
							throw new ProgressError(ex.toString(), person_id);
						} catch (LdapAttributeException ex) {
                            throw new ProgressError(ex.toString(), person_id);
                        } catch (NamingException ex) {
                            throw new ProgressError(ex.toString(), person_id);
                        }
                        // if that was successful, then we'll pass the current
						// security question on to the next page
						ChallengeData challenge_data = null;
						try {
							challenge_data = getChallengeData(school_code,
									username, person_id);
							if (challenge_data != null) {
								session.setAttribute("curr_question",
										challenge_data.getQuestion());
								session.setAttribute("curr_example",
										challenge_data.getExample());
							} else {
								session.setAttribute("curr_question", "");
								session.setAttribute("curr_example", "");
							}
							progress += 1;
							session.setAttribute("progress",
                                    progress);
							cntx.setAttribute("field_list", school_config
									.getFields(school_code,
											ConfigFieldType.VERIFY));
							next_page = VLD_PG;
						} catch (LockedAccountException e) {
							throw new ProgressError(e.toString(), person_id,
									ProgressErrorType.LOCKED);
						} catch (UniException e) {
							throw new ProgressError(
									"unable to access Colleague: ", person_id,
									e, ProgressErrorType.SYSTEM);
						}
					} else {
						// raise error since the progress does not match the
						// page
                        StringBuffer err_msg;
                        err_msg = new StringBuffer("session ").append(session.getId()).append(" in an invalid state").append(", expected 'start' got '").append(process_step).append("'");
						throw new ProgressError(err_msg.toString(), person_id, ProgressErrorType.SESSION_INVALID);
					}
					break;
				case 2:
					if (process_step.equals("validate")) {
						try {
							// step 2, the user has submitted validating
							// information, if it checks out, then show them the
							// change password page
							if (checkInfo(req, session)) {
								// step 3 the info matches, so display the
								// change
								// password page
								session.setAttribute("security_questions",
										Environment.security_questions);
								ChallengeData chd = getChallengeData(
										school_code, username, person_id);
								if (chd != null) {
									session.setAttribute("curr_question",
											chd.getQuestion());
								} else {
									session.setAttribute("curr_question", "");
								}
								// set attributes with the password requirements
								session.setAttribute("passwd_min_len",
										validator.getMinLength());
								session.setAttribute("passwd_max_len",
										validator.getMaxLength());
								session.setAttribute("passwd_no_special",
										validator.noSpecial());
								progress += 1;
								session.setAttribute("progress",
                                        progress);
								next_page = CHG_PG;
							}
						} catch (SystemUnavailableException e) {
							throw new ProgressError("fatal error", e,
									ProgressErrorType.SYSTEM);
						} catch (IllegalStateException e) {
							throw new ProgressError("session "
									+ session.getId()
									+ " timed out (illegal state)", person_id,
									ProgressErrorType.SESSION_INVALID);
						} catch (LockedAccountException e) {
							throw new ProgressError(e.toString(), person_id,
									ProgressErrorType.LOCKED);
						} catch (UniException e) {
							throw new ProgressError(e.toString(), person_id,
									ProgressErrorType.MISMATCH);
						}
					} else {
						// raise error since the progress does not match the page
						StringBuffer err_msg;
                        err_msg = new StringBuffer("session ").append(session.getId()).append(" in an invalid state").append(", expected 'validate' got '").append(process_step).append("'");
                        throw new ProgressError(err_msg.toString(), person_id, ProgressErrorType.SESSION_INVALID);
					}
					break;
				case 3:
					if (process_step.equals("change")) {
						// step 4, the session continues and we save the
						// password.
						try {
							String result = savePassword(req, session);
							// if the password successfully changed...
							if (result.isEmpty()) {
								// move some variables from the session to the
								// context
								cntx.setAttribute("warning",
										session.getAttribute("warning"));
								cntx.setAttribute("passwordhelp",
										session.getAttribute("passwordhelp"));
								// ...then generate the success message
								next_page = FIN_PG;
								// we're done with the whole process
								session.invalidate();
							} else {
								// If the password did not pass validation
								// then we redisplay the page with error
								// messages
								// show_full_error_page = false;
								next_page = CHG_PG;
								session.setAttribute("password_result", result);
							}
						} catch (IllegalStateException e) {
							throw new ProgressError(
									"Session invalid (illegal state)",
									person_id,
									ProgressErrorType.SESSION_INVALID);
						} catch (UniException e) {
							throw new ProgressError(
									"Failed to connecto to database",
									person_id, e, ProgressErrorType.SYSTEM);
						}
					} else {
						// raise error
                        StringBuffer err_msg;
                        err_msg = new StringBuffer("session ").append(session.getId()).append(" in an invalid state").append(", expected 'validate' got '").append(process_step).append("'");
                        throw new ProgressError(err_msg.toString(), person_id, ProgressErrorType.SESSION_INVALID);
					}
					break;
				default:
					throw new ProgressError(
							"Session invalid (invalid process step)",
							person_id, ProgressErrorType.SESSION_INVALID);
				}
				if (is_mobile) {
					next_page = "m_" + next_page;
				}
				RequestDispatcher rd = cntx.getRequestDispatcher("/" + next_page);
				if (rd != null) {
					try {
						rd.include(req, res);
					} catch (Exception e) {
						logMsg("include problem: " + e, "", Level.SEVERE);
					}
				} else {
					res.sendError(HttpServletResponse.SC_BAD_REQUEST,
							"Page not found:" + next_page);
				}
				if (msg.length() > 0) {
					StringBuffer html;
					html = new StringBuffer("<div id='errorMessage'>\n").append(msg).append("</div>\n");
					out.append(html.toString());
				}
				logMsg("doPost: Closing page normally for step " + process_step,
						person_id, Level.CONFIG);
			} catch (ProgressError e) {
				if (m_errors.containsKey(e.getErrorType())) {
					msg.append(m_errors.get(e.getErrorType()));
				} else {
					msg.append(m_errors.get(ProgressErrorType.MISMATCH));
				}
				// pass back the school code after an error
				StringBuffer link_params;
				link_params = new StringBuffer("?school_code=").append(school_code);
				// set up the link to include the parameter passwordhelp if it is set
				if (is_passwordhelp) {
					link_params.append("&passwordhelp=true");
				}
				cntx.setAttribute("link",
						req.getContextPath() + req.getServletPath()
								+ link_params.toString());
				cntx.setAttribute("debug", debug);
				cntx.setAttribute("error_msg", msg.toString());
				cntx.setAttribute("log_msg", e.toString());
				// Display the error JSP page
				String error_page = ERR_PG;
				if (is_mobile) { error_page = "m_" + error_page; }
				RequestDispatcher rd = cntx.getRequestDispatcher("/" + error_page);
				if (rd != null) {
					try {
						rd.include(req, res);
					} catch (Exception e2) {
						logMsg("include problem: " + e2, "", Level.SEVERE);
					}
				} else {
					res.sendError(HttpServletResponse.SC_BAD_REQUEST,
							"Page not found:" + next_page);
				}
				// doError(out, link, msg.toString(), e.toString(),
				// show_full_error_page);
				servlet_log.log(Level.INFO, "doPost: " + e.toString()
						+ " [step=" + process_step + "]");
				session.invalidate();
			}
			out.close();
		}
	}

	private Ldap connectLdap() {
		// set up a connection to LDAP
		return new Ldap(properties.getProperty("ldap.uri"),
				properties.getProperty("ldap.use_ssl"),
				properties.getProperty("ldap.ssl_validate_cert"),
				properties.getProperty("ldap.bind_dn"),
				properties.getProperty("ldap.password"),
				properties.getProperty("ldap.search_base"),
				properties.getProperty("ldap.username_field"));
	}

	private Environment connectDb() throws UniException {
		// Start the connection to UniData
		Environment db;
		if (local_params_filename.equals("")) {
			db = new Environment(properties.getProperty("db.server"),
					properties.getProperty("db.admin_user"),
					properties.getProperty("db.password"));
		} else {
			db = new Environment(properties.getProperty("db.server"),
					properties.getProperty("db.admin_user"),
					properties.getProperty("db.password"),
					local_params_filename);
		}
		db.connect(properties.getProperty("db.account_path"));

		db.openAll();
		return db;
	}

	private ChallengeData getChallengeData(String school_code, String username, String person_id) throws UniException, LockedAccountException {
		ChallengeData challenge_data = null;
		ArrayList<LocalConfigItem> fields = school_config.getFields(school_code, ConfigFieldType.CHALLENGE);
		if (!fields.isEmpty()) {
			LocalConfigItem lci = fields.get(0);
			String challenge_source = lci.getSource();
			if (challenge_source.equals("unidata")) {
				Environment db = connectDb();
				challenge_data = db.loadSecurityQuestion(person_id);
				// close the connection to Colleague now that we're done
				db.close();
				db.disconnect();
			} else if (challenge_source.equals("ldap")) {
				Ldap ld = connectLdap();
				challenge_data = ld.loadSecurityQuestion(username, lci.getAttribute());
			}
		}
		return challenge_data;
	}

	/**
	 * Check the Directory server to see if the username exists and person ID matches.
	 * 
	 * @param username A string with the users account name
	 * @param person_id A string with the numeric ID
	 * @return true if the person id for the given username matches
	 */
    private boolean checkUsername(String username, String person_id) throws ProgressError {
		boolean found = false;
		try {
			Ldap ld = connectLdap();
			String employee_number = ld.getEmployeeNumber(username);
			found = person_id.equals(employee_number);
		} catch (NoPersonIdException ex) {
			throw new ProgressError("ResetPassword.checkUsername, Person ID not found", person_id, ex);
		}
		return found;
	}

	/**
	 * Check UniData to see if the user has entered a valid username and matching Person
	 * ID.
	 * 
	 * <p>
	 * I read the ORG.ENTITY.ENV record and check to see if the username
	 * matches. If it does I can display the next page. Otherwise I will display
	 * an error page.
	 * 
	 * @param username
	 *            The contents of the username on the form.
	 * @param person_id
	 *            The contents of person id field on the form.
	 */
    private boolean checkUsernameDB(String username, String person_id)
			throws ProgressError {
		boolean found = false;
		try {
			Environment db = connectDb();
			OrgEntityEnv oee = db.getOEE(person_id);
			if (username.equals(oee.username.get())) {
				found = true;
			}
		} catch (UniSessionException ex) {
			throw new ProgressError("Error with the UniSession.", person_id, ex);
		} catch (UniFileException e) {
			throw new ProgressError(
					"ResetPassword.checkUsername, Person ID not found",
					person_id, e);
		} catch (NullPointerException e) {
			throw new ProgressError("Null Pointer for " + username, person_id,
					e);
		} catch (UniException e) {
			throw new ProgressError("Error with the UniSession.", person_id, e);
		}

		return found;
	}

	private boolean accountIsDisabled(String username) throws UidNotFoundException, LdapAttributeException, NamingException {
		boolean found = false;
		Ldap ld = connectLdap();
		found = ld.isDisabled(username);
        return found;
	}

	/**
	 * Processes and checks the information entered on the form on the index
	 * page. If everything matches the requester is validated as the authorized
	 * user and can proceed.
	 * 
	 * <p>
	 * If a secret answer has not been set in Colleague then any answer should
	 * be accepted and just propagated to the change password page.
	 * 
	 * @param req
	 *            the HTTP request object
	 * @param session
	 *            the HTTP session object
	 * @return true if all the information entered matched what was in the
	 *         database
	 * @throws org.cfcc.SystemUnavailableException
	 *             A fatal error if the encryption library is not present.
	 * @throws IllegalStateException
	 *             if the session times out before the user_dn can be set.
	 */
    private boolean checkInfo(HttpServletRequest req, HttpSession session)
			throws SystemUnavailableException, IllegalStateException,
			ProgressError {
		String person_id = (String) session.getAttribute("person_id");
		String username = (String) session.getAttribute("username");
		String school_code = (String) session.getAttribute("school_code");

		boolean found_match = false;
		String failed_on = "";

		try {

			Environment db = connectDb();
			Ldap ld = connectLdap();

			// Verify the answers the user has submitted against what is store in the database (or LDAP).
			ArrayList<LocalConfigItem> field_list = school_config.getFields(school_code, ConfigFieldType.VERIFY);
			for (LocalConfigItem item : field_list) {
				
				String value = Validate.sanitize(req.getParameter("txt_" + item.getName()));
				// LDAP data is sometimes encrypted, so we need to make sure the incoming 
				// value is a hash that can be compared to the expected value's hash. 
				if (item.isEncrypted()) {
			        LdapShaPasswordEncoder pwd = new LdapShaPasswordEncoder();
			        value = pwd.encodePassword(value, null);
				}
				String exp_value = "";
				if (item.getSource().equals("unidata")) {
					exp_value = db.getField(item.getTable(), item.getLocation(), person_id, item.getConv());
				} else if (item.getSource().equals("unidata foreign key")) {
					exp_value = db.getRemoteField(item.getTable(), item.getLocation(), item.getRemoteTable(), item.getRemoteLocation(), person_id);
				} else if (item.getSource().equals("ldap")) {
                    try {
                        exp_value = ld.getAttributeValue(username, item.getAttribute());
                    } catch (NamingException e) {
                        servlet_log.log(Level.WARNING, "checkInfo: " + e.toString());
                    } catch (LdapAttributeException e) {
                        servlet_log.log(Level.WARNING, "checkInfo: " + e.toString());
                    }
                }
				// Trim the expected value that we retrieved from the DB, unless it's known to be encrypted
				if (exp_value != null) {
					if (exp_value.length() > 0) {
						if (item.isNumeric()) {
							exp_value = Validate.numbersOnly(exp_value, item.getLength()); 
						} else if (!item.isEncrypted()) {
							exp_value = Validate.sanitize(exp_value, item.getLength());
						}
					}
					System.err.println("[DEBUG] " + item.getName() + ", exp_value = " + exp_value + ", value = " + value);
					// If the expected value is blank, then instead of failing the check
					// we'll accept any answer as correct. This is mainly because SSN's
					// are not required by the institution (though most people give them).  
					if (exp_value.equals("") || value.equals(exp_value)) {
						found_match = true;
					} else {
						found_match = false;
						failed_on = item.getName();
						break;
					}
				} else {
					found_match = false;
					failed_on = item.getName();
					break;
				}
			}

			// if everything has passed so far, then we'll check the secret
			String secret = Validate.sanitize(req.getParameter("txt_secret"), Validate.SECRET_LEN);
			if (found_match) {
				String exp_secret = "";
				ChallengeData challenge_data = getChallengeData(school_code, username, person_id);
				if (challenge_data != null) {
					exp_secret = challenge_data.getSecret();
				}

				System.err.println("[DEBUG] exp_secret = " + exp_secret + ", secret = " + secret);

				found_match = Validate.checkSecret(secret, exp_secret);
				if (!found_match) { failed_on = "challenge answer"; }
			}
			if (found_match) {
				// this will show up as the default secret, I use the one
				// typed in on the form (if any) since the one in the DB
				// is already hashed and we can't get it back.
				session.setAttribute("secret", secret);
				logMsg("Validation for '" + username + "' succeeded",
						person_id, Level.INFO);
			} else {
				throw new ProgressError("Reseting password for '"
						+ username + "' failed, " + failed_on
						+ " was incorrect.", person_id);
			}
		} catch (LockedAccountException e) {
			throw new ProgressError(e.toString(), person_id,
					ProgressErrorType.LOCKED);
		} catch (UniException e) {
			throw new ProgressError("Database connection failed.", person_id,
					e, ProgressErrorType.SYSTEM);
		}
		return found_match;
	}
	
	/**
	 * Saves the password to LDAP and the challenge SECRET to UniData (or LDAP)
	 * 
	 * @param req
	 *            the HTTP request object
	 * @param session
	 *            the HTTP session
	 * @return msg_result a String with any errors, blank if successful
	 * @throws UniException
	 *             if the database connection fails
	 */
    private String savePassword(HttpServletRequest req, HttpSession session)
			throws ProgressError, UniException {
		/*
		 * NOTE: I am intentionally _not_ sanitizing these 4 parameters so that
		 * the data is saved exactly as the user entered it. The data is stored
		 * as text and read back as text, so there should be no issues with it
		 * being misinterpreted as code.
		 */
		String passwd = req.getParameter("txt_passwd");
		String passwd_check = req.getParameter("txt_passwd_check");
		String question = req.getParameter("sel_question");
		String secret = req.getParameter("txt_secret");

		// Update the secret field in the session just in case the password does
		// not pass validation
		session.setAttribute("secret", secret);
		session.setAttribute("curr_question", question);
		
		String person_id = (String) session.getAttribute("person_id");
		String username = (String) session.getAttribute("username");

		StringBuffer msg_result = new StringBuffer("");
		
		String school_code = (String) session.getAttribute("school_code");
		ArrayList<LocalConfigItem> fields = school_config.getFields(school_code, ConfigFieldType.CHALLENGE);
		LocalConfigItem lci = fields.get(0);

		String challenge_source = lci.getSource();

		Ldap ld = connectLdap();

		Environment db = connectDb();

		if (username != null) {
			ArrayList<String> result = validator.checkPassword(passwd,
					passwd_check);
			// We also need to check if this password has been used before
			if (challenge_source.equals("unidata")) {
				if (db.checkPasswordHistory(person_id, passwd)) {
					result.add(m_errors.get(ProgressErrorType.PASSWORD_HISTORY));
				}
			}
			// now, if there are no error messages, we can continue saving the
			// password to the database.
			if (result.isEmpty()) {
				// first set the password
				try {
					ld.setLastDn(ld.getDN(username));
					if (use_unicode_password) {
						ld.setUnicodePassword(passwd);
					} else {
						try {
							String old_password = ld.getPassword();
							// ** Send the new password to LDAP **
							ld.setPassword(passwd, use_password_salt);
							// with OpenLDAP and LOCAL.PARAMS we can save the
							// previous password to prevent the user from
							// reusing it on the next reset.
							if (challenge_source.equals("unidata")) {
								db.setPasswordHistory(person_id, old_password);
								logMsg("Old password saved in the history field",
										person_id, Level.INFO);
							}
						} catch (UnsupportedEncodingException ex) {
							Logger.getLogger(ResetPassword.class.getName())
									.log(Level.WARNING,
											"Unable to save the previous password",
											ex);
						} catch (UniFileException ex) {
							Logger.getLogger(ResetPassword.class.getName())
									.log(Level.WARNING,
											"Unable to save the previous password",
											ex);
						} catch (UniSessionException ex) {
							Logger.getLogger(ResetPassword.class.getName())
									.log(Level.WARNING,
											"Unable to save the previous password",
											ex);
						}
					}
                    // Now set the gmail password if the properties file has the
                    // configuration
//                    if (properties.containsKey("google.domain") == true) {
//                    	logMsg("Sending password update to Google Apps", person_id, Level.INFO);
//                    	GaUpdate ga_thread = new GaUpdate(
//                                properties.getProperty("google.admin_email"),
//                                properties.getProperty("google.admin_password"),
//                                properties.getProperty("google.domain"),
//                                ld.getLastUsername(),
//                                passwd,
//                                person_id);
//                        if ("yes".equals(properties.getProperty("google.separate_thread", "no"))) {
//                            Thread thr = new Thread(ga_thread);
//                            thr.start();
//                        } else {
//                            ga_thread.run();
//                        }
//                    }
					// then save the secret
            		if (secret.compareTo(req.getParameter("txt_secret")) != 0) {
            			// FIXME: Should we throw an exception here?
            			session.setAttribute("warning", m_errors.get(ProgressErrorType.INVALID_SECRET)
            					+ "<pre>" + secret + "</pre>");
            			logMsg("Secret string contained invalid charcters ["
            					+ secret + "]!=["
            					+ req.getParameter("txt_secret") + "]",
            					person_id, Level.WARNING);
            		}
            		if (challenge_source.equals("unidata")) {
            			try {
            				db.saveChallenge(person_id, question, secret);
        					// update the password change date for WebAdvisor
        					db.setPasswordDate(person_id);
            			} catch (SystemUnavailableException e) {
            				throw new ProgressError("Unable to save secret",
            						person_id, e, ProgressErrorType.SECRET_FAILED);
            			} catch (UniFileException e) {
            				throw new ProgressError("Unable to save secret",
            						person_id, e, ProgressErrorType.SECRET_FAILED);
            			} catch (UniSessionException e) {
            				throw new ProgressError("Unable to save secret",
            						person_id, e, ProgressErrorType.SECRET_FAILED);
            			}
            		} else if (challenge_source.equals("ldap")) {
            			ld.saveChallenge(username, lci.getAttribute(), question, secret);
            		}
					logMsg("Saved password for '" + ld.getLastUsername() + "'",
							person_id, Level.INFO);
				} catch (PasswordConstraintException e) {
					throw new ProgressError(
							"Password does not meet constraints: " + e,
							person_id, ProgressErrorType.BAD_PASSWORD);
				} catch (LdapChangeException e) {
					throw new ProgressError("LDAP: " + e, person_id,
							ProgressErrorType.FAILURE);
				} catch (SystemUnavailableException e) {
					throw new ProgressError("Encryption error: " + e,
							person_id, ProgressErrorType.FAILURE);
				} catch (StringIndexOutOfBoundsException e) {
					throw new ProgressError("LDAP: " + e, person_id,
							ProgressErrorType.FAILURE);
				} catch (NamingException e) {
					throw new ProgressError("LDAP: " + e, person_id,
							ProgressErrorType.FAILURE);
				}
			} else {
				ListIterator<String> errors = result.listIterator();
				msg_result.append("<ul>");
				while (errors.hasNext()) {
					msg_result.append("<li>").append(errors.next()).append("</li>");
				}
				msg_result.append("</ul>");
				logMsg("ResetPassword: password failed validation "
						+ ld.getLastUsername(), person_id, Level.WARNING);
			}
		} else {
			throw new ProgressError(
					"failed to retrieve user name from session "
							+ session.getId(), person_id);
		}
		return msg_result.toString();
	}
	
	private boolean isThisRequestCommingFromAMobileDevice(HttpServletRequest request){

	    // http://www.hand-interactive.com/m/resources/detect-mobile-java.htm
	    String userAgent = request.getHeader("User-Agent");
	    String httpAccept = request.getHeader("Accept");

	    UAgentInfo detector = new UAgentInfo(userAgent, httpAccept);

        return detector.detectMobileQuick();

    }
	
	/**
	 * Central function to print log messages.
	 * 
	 * @param msg
	 *            The message to print.
	 * @param track_id
	 *            The person ID is used to track the session
	 * @param level
	 *            The reporting level (ex: Error, Warning, Info, etc)
	 */
	public void logMsg(String msg, String track_id, Level level) {
		/*
		 * The cntx.log does not seem to work ServletContext cntx =
		 * getServletContext(); cntx.log("ResetPassword: [" + level +
		 * "] (track_id=" + track_id + ") " + msg);
		 */
		StringBuffer log_msg = new StringBuffer("");
		try {
			log_msg.append("(track_id=").append(track_id).append(") ");
		} catch (NullPointerException ex) {
			log_msg.append("(track_id=NONE) error=").append(ex.toString());
		}
		log_msg.append(msg);
		servlet_log.log(level, log_msg.toString());
		//System.out.println(log_msg.toString());
	}

	/**
	 * Alternate function to print log messages with an exception included.
	 * 
	 * @param msg
	 *            The message to print.
	 * @param ex
	 *            The originating exception.
	 * @param track_id
	 *            The person ID is used to track the session
	 * @param level
	 *            The reporting level (ex: Error, Warning, Info, etc)
	 */
	public void logMsg(String msg, Throwable ex, String track_id, Level level) {
		StringBuffer log_msg = new StringBuffer("");
		try {
			log_msg.append("(track_id=").append(track_id).append(") ");
		} catch (NullPointerException ex2) {
			log_msg.append("(track_id=NONE) error=").append(ex2.getMessage());
		}
		log_msg.append(msg).append(" (").append(ex.getMessage()).append(")");
		servlet_log.log(level, log_msg.toString(), ex);
		//System.out.println(log_msg.toString());
	}

	/**
	 * basic information function for use by the server
	 * 
	 * @return an identifying string
	 */
	@Override
	public String getServletInfo() {
		return "ResetPassword 4";
	}
}
