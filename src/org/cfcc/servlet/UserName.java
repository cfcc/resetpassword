package org.cfcc.servlet;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cfcc.DuplicateRecordException;
import org.cfcc.colleague.Environment;
import org.cfcc.utils.Validate;
import org.json.JSONException;
import org.json.JSONObject;

import asjava.uniclientlibs.UniException;

/**
 * Servlet implementation class UserName
 */
// @WebServlet(description = "Return the username", urlPatterns = { "/UserName" })
public class UserName extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/** Default properties for the Servlet configuration */
	private Properties default_props;
	/** Runtime properties that are loaded at each call */
	private Properties properties;
	//private static final Logger servlet_log = Logger.getLogger(UserName.class.getName());
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserName() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#init(ServletConfig config)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		// Load the Servlet configuration from the default properties file
		try {
			loadDefaultProperties();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Environment db;
		boolean success = false;
		String username = "";
		String person_id = "";
		try {
			loadProperties();

			db = connect();
			String last_name = Validate.sanitize(request.getParameter("per_last"));
			person_id = Validate.sanitize(request.getParameter("per_id"));
			String person_ssn = Validate.sanitize(request.getParameter("per_ssn"));
			if (!person_ssn.isEmpty()) {
				person_id = db.getPersonBySsn(person_ssn, false);
			}
			username = db.getUsername(person_id, last_name);
			if (!username.isEmpty()) {
				success = true;
			}
		} catch (UniException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DuplicateRecordException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PrintWriter out = response.getWriter();
		String result = "";
		Boolean as_html = Boolean.valueOf(Validate.sanitize(request.getParameter("asHtml")));
		if (as_html.booleanValue() == true) {
			ServletContext cntx = getServletContext();
			RequestDispatcher rd = cntx.getRequestDispatcher("/m_username.jsp");
			cntx.setAttribute("success", Boolean.valueOf(success));
			cntx.setAttribute("username", username);
			cntx.setAttribute("link", request.getContextPath() + "/ResetPassword");
			if (rd != null) {
				try {
					rd.include(request, response);
				} catch (Exception ex) {
					Logger.getLogger(UserName.class.getName()).log(Level.WARNING,
							"Error including 'm_username.jsp' with RequestDispatcher",
							ex);
				}
			} else {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST,
						"Page not found: m_username.jsp");
			}
		} else {
			response.setContentType("application/json");
			try {
				result = new JSONObject().put("success", success).put("username", username).put("person_id", person_id).toString();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			out.write(result);
		}
		out.close();
	}
	
	/*
	 * Default properties are compiled into the WAR file and only loaded when
	 * the servlet is deployed.
	 */
	protected void loadDefaultProperties() throws IOException {
		String mutex = "";
		synchronized (mutex) {
			ServletContext cntx = getServletContext();

			// create and load default properties
			default_props = new Properties();
			default_props.load(cntx.getResourceAsStream("/WEB-INF/default.properties"));
		}
	}

	/*
	 * Use the same ResetPassword.properties file as the ResetPassword servlet.
	 */
	protected void loadProperties() throws FileNotFoundException, IOException {
		String mutex = "";
		synchronized (mutex) {
			// create application properties with defaults
			properties = new Properties(default_props);

			// load local properties from the system
			String prop_path = default_props.getProperty("properties_path");
			properties.load(new FileInputStream(prop_path));
		}
	}

	protected Environment connect() throws UniException {
		// Start the connection to UniData
		Environment db;

		db = new Environment(properties.getProperty("db.server"),
				properties.getProperty("db.admin_user"),
				properties.getProperty("db.password"));

		db.connect(properties.getProperty("db.account_path"));

		db.openAll();
		return db;
	}
}
