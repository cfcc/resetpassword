/**
 * 
 */
package org.cfcc.servlet;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

import org.cfcc.servlet.LocalConfig;
import org.cfcc.servlet.LocalConfigItem;
import org.junit.Before;
import org.junit.Test;

/**
 * @author jfriant
 *
 */
public class TestLocalConfig {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for {@link org.cfcc.servlet.LocalConfig#LocalConfig(java.util.Properties)}.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@Test
	public void testLocalConfigProperties() throws FileNotFoundException, IOException {
		String prop_path = "/home/jfriant/workspace/ResetPassword/custom/school.properties";
		Properties props = new Properties();
		props.load(new FileInputStream(prop_path));
		LocalConfig cfg = new LocalConfig(props);
		
		String[] expected_names = {"cfcc", "ncwc"};
		Set<String> school_names = cfg.getSchoolTitle().keySet();
		assertArrayEquals(expected_names, school_names.toArray());

		System.out.println("SchoolFields");
		Set<String> school_codes = cfg.getSchoolCodes();
		for (String school_name : school_codes) {
			System.out.println(school_name);
			for (ConfigFieldType field_type : ConfigFieldType.values()) {
				ArrayList<LocalConfigItem> school_fields = cfg.getFields(school_name, field_type);
				for (LocalConfigItem field : school_fields) {
					System.out.println(
							field.getType().toString() + ", "
							+ field.getName() + ", "
							+ field.getSource() + ", "
							+ field.getLength() + ", "
							+ field.getExample() + ", "
							+ field.getError() + ", "
							+ field.isNumeric());
				}
			}
		}
	}
	
	@Test
	public void testLocalConfigJavascript() throws Exception {
		System.out.println("localConfigJavascript");

		String prop_path = "/home/jfriant/workspace/ResetPassword/custom/school.properties";
		Properties props = new Properties();
		props.load(new FileInputStream(prop_path));
		LocalConfig cfg = new LocalConfig(props);

		String result = cfg.getSchoolInfoAsJS();
		assertNotNull(result);
		System.out.println(result);
	}

}
